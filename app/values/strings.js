const BASE_URL_LOCAL = "http://localhost:8443/";
const BASE_URL_SERVER = "https://rimtal.com/";

module.exports = {
    BASE_URL_LOCAL, BASE_URL_SERVER
};