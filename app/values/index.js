const errors = require('./errors');
const configs = require('./configs');
const strings = require('./strings');
const success = require('./success');
const constants = require('./constants');
module.exports = {errors, configs, strings, success, constants};