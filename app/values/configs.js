const MONGOOSE_USR = "rimtalAdmin";
const MONGOOSE_PWD = `r!mtala3m!n2021`;
const MONGOOSE_PORT = "27017";
const MONGOOSE_IP = "45.89.139.227";
const MONGOOSE_DATABASE_NAME = "RimtalDb";
const MONGOOSE_CONNECTION_URL = `mongodb://${MONGOOSE_USR}:${MONGOOSE_PWD}@${MONGOOSE_IP}:${MONGOOSE_PORT}/${MONGOOSE_DATABASE_NAME}`;
const MONGOOSE_CONFIG = {
    useNewUrlParser: true,
    authSource: MONGOOSE_DATABASE_NAME,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true
};
const PATH_BASE_PHOTO = `./images/base`;
const PATH_SLIDER_PHOTO = `./images/slider`;
const PATH_ALBUM_PIC = `./images/album`;
const PATH_ARTIST_AVATAR = `./images/artist`;
const PATH_SONG_PIC = `./images/song`;
const PATH_MUSIC_VIDEO_PIC = `./images/musicvideo`;
const PATH_USER_AVATAR = `./images/user`;
const PATH_FLAG = `./images/flag`;
const PATH_PLAYLIST_PIC = `./images/playlist`;
const PATH_INSTRUMENT_THUMBNAIL = `./images/instrument-thumbnail`;
const PATH_MOOD_PICTURE = `./images/mood`;


const KN_API_KEY = "69685652716A74744C587530514A31363354624B6152544C4D364D70317773616C7944664A3365494E776B3D";
const KN_PANEL_URL = `https://api.kavenegar.com/v1/${KN_API_KEY}/verify/lookup.json`;

const TOKEN_SECRET = "R!mT@L2020";

// Picture Size Configs

// Slider
const SLIDER_PHONE = {width: 260, height: 150};
const SLIDER_WEB = {width: 540, height: 310};

//Artist
const ARTIST_WEB = {width: 200, height: 200};
const ARTIST_PHONE = {width: 150, height: 150};

//Thumbnail
const THUMBNAIL_PHONE = {width: 150, height: 150};
const THUMBNAIL_WEB = {width: 180, height: 180};

//Video
const VIDEO_THUMBNAIL_PHONE = {width: 260, height: 150};
const VIDEO_THUMBNAIL_WEB = {width: 360, height: 180};

//MOOD
const MOOD_PHONE = {width:180 , height:85};
const MOOD_WEB = {width: 250, height: 120};

//User
const USER_WEB ={width: 200, height: 200}
const USER_PHONE ={width: 150, height: 150}


// Phone
const SEARCH_THUMBNAIL_APP = {width: 50, height: 50};
const PLAYER_APP = {width: 250, height: 250};
const PLAYER_WITH_DETAILS_APP = {width: 150, height: 150};


// Website
const USER_THUMBNAIL_WEB = {width: 35, height: 35};

const ALBUM_THUMBNAIL_WEB = {width: 180, height: 180};

const ARTIST_PAGE_AVATAR_WEB = {width: 200, height: 200};
const ARTIST_PAGE_LATEST_WEB = {width: 300, height: 300};
const ARTIST_PAGE_TRACKS_WEB = {width: 50, height: 50};
const ARTIST_PAGE_SIMILAR_AVATAR_WEB = {width: 80, height: 80};
const ARTIST_ALBUMS_IMAGE_WEB = {width: 173.03, height: 173.03};

const ARTIST_VIDEOS_WEB = {width: 319.15, height: 159.56};

const FLAG_WEB = {width: 250, height: 180};
const FLAG_PHONE = {width: 200, height: 130};
const INSTRUMENT_WEB = {width: 158, height: 158};
const INSTRUMENTS_PHONE = {width: 74, height: 61};
const PLAYLIST_MAIN_WEB = {width: 240, height: 240};

const PLAYER_MAIN_WEB={width: 250, height: 250};
const PLAYER_SONG_THUMBNAIL_WEB = {width: 35, height: 35};

const SEARCH_VIDEO_THUMBNAIL_WEB = {width: 300, height: 150};

const SINGLE_IMAGE_BUY_WEB = {width: 240, height: 240};

const VIDEO_MAIN_THUMBNAIL_WEB = {width: 535, height: 315};
const VIDEO_PAGE_THUMBNAIL_WEB = {width: 360, height: 180};



module.exports = {
    MONGOOSE_USR,
    MONGOOSE_PWD,
    MONGOOSE_IP,
    MONGOOSE_DATABASE_NAME,
    MONGOOSE_CONNECTION_URL,
    MONGOOSE_CONFIG,
    PATH_BASE_PHOTO,
    PATH_SLIDER_PHOTO,
    PATH_ALBUM_PIC,
    PATH_ARTIST_AVATAR,
    PATH_SONG_PIC,
    PATH_MUSIC_VIDEO_PIC,
    PATH_USER_AVATAR,
    PATH_FLAG,
    PATH_PLAYLIST_PIC,
    PATH_MOOD_PICTURE,
    PATH_INSTRUMENT_THUMBNAIL,
    KN_PANEL_URL,
    TOKEN_SECRET,
    SLIDER_PHONE,
    SLIDER_WEB,
    THUMBNAIL_PHONE,
    THUMBNAIL_WEB,
    VIDEO_THUMBNAIL_PHONE,
    VIDEO_THUMBNAIL_WEB,
    MOOD_PHONE,
    MOOD_WEB,
    INSTRUMENT_WEB,
    INSTRUMENTS_PHONE,
    ARTIST_WEB,
    ARTIST_PHONE,
    FLAG_WEB,
    FLAG_PHONE,
    USER_WEB,
    USER_PHONE
};