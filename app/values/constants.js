const ALBUM = "album";
const PLAY_LIST = "playlist";
const SONG = "song";
const FLAG = "flag";
const ARTIST = "artist";
const INSTRUMENT_THUMBNAIL = "instrument";
const MUSIC_VIDEO_PIC = "musicvideo";
const MOOD_PICTURE = "mood";
const SLIDER = "slider";

const constants = {
  ALBUM,
  PLAY_LIST,
  SONG,
  FLAG,
  ARTIST,
  INSTRUMENT_THUMBNAIL,
  MUSIC_VIDEO_PIC,
  MOOD_PICTURE,
  SLIDER
};

module.exports = constants;
