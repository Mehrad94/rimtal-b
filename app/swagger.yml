openapi: '3.0.2'

info:
  title: Rimtal API
  version: '1.3.0'
  description: Documentation of Rimtal API

servers:
  - url: https://rimtal.com/api/v1
  - url: http://localhost:8443/api/v1

tags:
- name: Admin
  description: Admin Routes
  externalDocs:
    description: Admin Routes Base URL
    url: https://rimtal.com/api/v1/admin/
- name: Website
  description: User Website Routes
  externalDocs:
    description: User Website Routes Base URL
    url: https://rimtal.com/api/v1/
- name: WebsiteFa
  description: User Website Persian Routes
  externalDocs:
    description: User Website Persian Routes Base URL
    url: https://rimtal.com/api/v1/
- name: Application
  description: User Application Routes
  externalDocs:
    description: User Application Routes Base URL
    url: https://rimtal.com/api/v1/
- name: ApplicationFa
  description: User Application Persian Routes
  externalDocs:
    description: User Application Persian Routes Base URL
    url: https://rimtal.com/api/v1/


paths:
#Admin routes

   /admin/album:
     get:
       tags:
         - Admin
       summary: returns albums
       description: Returns albums with data
       responses:
          200:
             description: Successful operation
             content:
               application/json:
                 schema:
                   type: array
                   items:
                     $ref: '#/components/schemas/Album'
          500: 
             description: 1099 = Something is wrong

     post:
       tags:
         - Admin
       summary: "adds an album"
       description: "Adds an album"
       requestBody:
         content:
           application/json:
             schema:
               $ref: '#/components/schemas/Album'
         required: true
       responses:
         201:
           description: 2020 = Album added
         406:
           description: 1011 = Wrong parameters
         500:
           description: 1099 = Something is wrong

   /admin/album/{id}:
     put:
       tags:
         - Admin
       summary: Edit an album
       description: Edit an ablum
       parameters:
         - name: id
           in: path
           required: true
           schema:
             type: string
       requestBody:
         content:
           application/json:
             schema:
               $ref: '#/components/schemas/Edit'
         required: true
       responses:
         200:
           description: 2024 = Album edited
         406:
           description: 1011 = Wrong parameters
         500:
           description: 1099 = Something is wrong

     delete:
       tags:
         - Admin
       summary: Delete an album
       description: Delete an album
       parameters:
         - name: id
           in: path
           required: true
           schema:
             type: string
       responses:
         204:
           description: 2013 = Album deleted
         500:
           description: 1099 = Something is wrong

   /admin/album/{page}:
     get:
       tags:
         - Admin
       summary: "returns albums with pagination"
       description: "Returns albums with data"
       parameters:
         - name: page
           in: path
           required: true
           schema:
             type: string
       responses:
         200:
           description: Successful operation
           content:
             application/json:
               schema:
                 type: array
                 items:
                   $ref: '#/components/schemas/Album'
         500:
           description: 1099 = Something is wrong
    
   /admin/artist/{page}:
    get:
      tags:
        - Admin
      summary: "returns artists"
      description: "Returns artist with pagination"
      parameters:
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Artist'
        500:
          description: 1099 = Something is wrong

   /admin/artist/search/{name}/{page}:
    get:
      tags:
        - Admin
      summary: "search for artists"
      description: "Search for artist with pagination"
      parameters:
        - name: name
          in: path
          description: artist name you wanna search for
          required: true
          schema:
            type: string
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Artist'
        500:
          description: 1099 = Something is wrong

   /admin/artist:
    post:
      tags:
        - Admin
      summary: "adds an artist"
      description: "Adds an artist"
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Artist'
        required: true
      responses:
        201:
          description: 2016 = Artist added
        406:
          description: 1011 Wrong parameters
        500:
          description: 1099 = Something is wrong

   /admin/artist/{id}:
    put:
      tags:
        - Admin
      summary: edit an artist
      description: Edit an artist
      parameters:
        - name: id
          in: path
          description: Artist ID to edit
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Edit'
      responses:
        200:
          description: 2027 = Artist Edited
        500:
          description: 1099 = Something is wrong

    delete:
      tags:
        - Admin
      summary: "deletes an artist"
      description: "Deletes an artist"
      parameters:
        - name: id
          in: path
          description: Artist ID to delete
          required: true
          schema:
            type: string
      responses:
        204:
          description: 2017 = Successful operation
        500:
          description: 1099 = Something is wrong

   /admin/upload/image:
    post:
      tags:
        - Admin
      summary: "uploads an image"
      description: "Uploads an image"
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                imageType:
                  type: string
                  $ref: '#/components/schemas/ImageType'
                imageName:
                  type: string
                image:
                  type: string
                  format: binary
              required:
                - imageType
                - image
      responses:
        200:
          description: Successful operation
        500:
          description: Something is wrong

   /admin/gallery/{type}/{page}:
    get:
      tags:
        - Admin
      summary: "search in images gallery"
      description: "Search for images with pagination"
      parameters:
        - name: type
          in: path
          description: image type
          required: true
          schema:
            type: string
            $ref: '#/components/schemas/ImageType'
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
          description: Successful operation
        500:
          description: Something is wrong

   /admin/upload/song:
    post:
      tags:
        - Admin
      summary: uploads an song
      description: Uploads an song
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                songName:
                  type: string
                song:
                  type: string
                  format: binary
              required:
                - song
      responses:
        200:
          description: Successful operation
        500:
          description: Something is wrong

   /admin/songlibrary/{page}:
    get:
      tags:
        - Admin
      summary: returns all songs with pagination
      description: Returns all songs with pagination
      parameters:
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
         description: Successful operation
        500:
         description: Something is wrong
 
   /admin/instrument:
    post:
      tags:
        - Admin
      summary: adds an instrument
      description: Adds an instrument
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Instrument'
        required: true
      responses:
        201:
          description: 2014 = Instrument added
        406:
          description: 1011 Wrong parameters
        500:
          description: 1099 = Something is wrong

   /admin/instrument/{page}:
    get:
      tags:
        - Admin
      summary: returns all instruments
      description: Returns all instruments
      parameters:
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
         description: Successful operation
         content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/Instrument'
        500:
         description: 1099 = Something is wrong
        
   /admin/instrument/{name}/{page}:
    get:
      tags:
        - Admin
      summary: returns searched instruments with pagination
      description: Returns searched instruments with pagination
      parameters:
        - name: name
          in: path
          description: instrument name you wanna search for
          required: true
          schema:
            type: string
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
         description: Successful operation
         content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/Instrument'
        500:
         description: 1099 = Something is wrong

   /admin/hashtag:
    post:
      tags:
        - Admin
      summary: creates a new hashtag
      description: Creates a new hashtag
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Hashtag'
        required: true
      responses:
        201:
          description: 2028 = Hashtag created
        406:
          description: 1011 Wrong parameters
        500:
          description: 1099 = Something is wrong       

   /admin/hashtag/{page}:
    get:
      tags:
        - Admin
      summary: returns all hashtags
      description: Returns all hashtags
      parameters:
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
         description: Successful operation
         content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/Hashtag'
        500:
         description: 1099 = Something is wrong        

   /admin/mood/{page}:
    get:
      tags:
        - Admin
      summary: returns all moods
      description: Returns all moods
      parameters:
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
         description: Successful operation
         content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/Mood'
        500:
         description: 1099 = Something is wrong 

   /admin/mood/{title}/{page}:
    get:
      tags:
        - Admin
      summary: returns searched moods with pagination
      description: Returns searched moods with pagination
      parameters:
        - name: title
          in: path
          description: mood's title you wanna search for
          required: true
          schema:
            type: string
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
         description: Successful operation
         content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/Mood'
        500:
         description: 1099 = Something is wrong

   /admin/mood:
    post:
      tags:
        - Admin
      summary: creates a new mood
      description: Creates a new mood
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Mood'
        required: true
      responses:
        201:
          description: 2029 = Mood created
        406:
          description: 1011 Wrong parameters
        500:
          description: 1099 = Something is wrong  
      
   /admin/country:
    post:
      tags:
        - Admin
      summary: adds a new country
      description: Adds a new country
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Country'
        required: true
      responses:
        201:
          description: 2011 = Country created
        406:
          description: 1011 Wrong parameters
        500:
          description: 1099 = Something is wrong              

   /admin/country/{page}:
    get:
      tags:
        - Admin
      summary: returns all countries with pagination
      description: Returns all countries with pagination
      parameters:
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
         description: Successful operation
         content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/Country'
        500:
         description: 1099 = Something is wrong

   /admin/country/{countryName}/{page}:
    get:
      tags:
        - Admin
      summary: returns searched countries pagination
      description: Returns searched countries pagination
      parameters:
        - name: countryName
          in: path
          required: true
          schema:
            type: string
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
         description: Successful operation
         content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/Country'
        500:
         description: 1099 = Something is wrong

#Application En

   /splashscreen:
    get:
      tags:
        - Application
      summary: "returns splash screen data"
      description: "Returns splash screen data"
      responses:
        200:
          description: Successful operation
        500:
          description: Something is wrong

#Application Fa

   /splashscreen/fa:
    get:
      tags:
        - ApplicationFa
      summary: "returns splash screen data"
      description: "Returns splash screen data"
      responses:
        200:
          description: Successful operation
        500:
          description: Something is wrong
 
#Website En

   /home:
    get:
      tags:
        - Website
      summary: "returns homepage data"
      description: "Returns homepage data"
      responses:
        200:
          description: Successful operation
        500:
          description: 1099 = Something is wrong

   /albums/{page}:
    get:
      tags:
        - Website
      summary: shows all albums
      description: Shows all albums
      parameters:
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
          description: Successful operation
        500:
          description: 1099 = Something is wrong

    post:
      tags:
        - Website
      summary: show all albums with filtering, sorting and pagination  
      description: show all albums with filtering, sorting and pagination
      parameters:
        - name: page
          in: path
          required: true
          schema:
            type: integer
            format: int64
      requestBody:
          required: true
          content:
            application/json:
              schema:
                type: object
                properties:
                  filters:
                    type: array
                    items:
                     type: object
                     anyOf:
                        - $ref: '#/components/schemas/FilterByTimeWithDays'
                        - $ref: '#/components/schemas/FilterByTimeWithinAPeriod'
                        - $ref: '#/components/schemas/FilterByGenre'
                        - $ref: '#/components/schemas/FilterByInstrument'
                        - $ref: '#/components/schemas/FilterByHashtag'
                        - $ref: '#/components/schemas/FilterByMood'
                  sort:
                    type: object
                    properties:
                      type:
                        type: string
                        $ref: '#/components/schemas/Sort'
      responses:
         200:
           description: Successful operation
           content:
             application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Album'
         500:
           description: 1099 = Something is wrong

#Website Fa

   /home/fa:
    get:
      tags:
        - WebsiteFa
      summary: "returns homepage data"
      description: "Returns homepage data"
      responses:
        200:
          description: Successful operation
        500:
          description: Something is wrong


components:
  schemas:

    Artist:
      required:
        - nameFa
        - nameEn
        - descriptionEn
        - descriptionFa
        - defaultGenre
      type: object
      properties:
        nameFa:
          type: string
          example: ریحانا
        nameEn:
          type: string
          example: Rihanna
        avatar:
          $ref: '#/components/schemas/Images'
        descriptionFa:
          type: string
        descriptionEn:
          type: string
        birthday:
          $ref: '#/components/schemas/Birthday'
        defaultGenre:
          $ref: '#/components/schemas/Genre'
        genres:
          $ref: '#/components/schemas/Genre'
        birthPlaceCity:
          type: string
        birthPlaceCountry:
          $ref: '#/components/schemas/Country'
        gender:
          type: string
          enum:
            - MALE
            - FEMALE
        instruments:
          $ref: '#/components/schemas/Instrument'
        website:
          type: string
        instagram:
          type: string
        youTube:
          type: string
        twitter:
          type: string
 
    Album:
      type: object
      properties:
        titleFa:
          type: string
          example: نام آلبوم
        titleEn:
          type: string
          example: Album's Title
        artist:
          $ref: '#/components/schemas/Artist'
        images:
          $ref: '#/components/schemas/Images'
        songs:
          $ref: '#/components/schemas/Song'
        moods:
          $ref: '#/components/schemas/Mood'
        instruments:
          $ref: '#/components/schemas/Instrument'
        hashtags:
          $ref: '#/components/schemas/Hashtag'
        countries:
          $ref: '#/components/schemas/Country'
        genres:
          $ref: '#/components/schemas/Genre'
        released:
          type: boolean

    Birthday:
      type: object
      properties:
        year:
          type: string
        month:
          type: string
        day:
          type: string

    Country:
      type: object
      properties:
        titleFa:
          type: string
          example: نام کشور
        titleEn:
          type: string
          example: Country's Title
        descriptionFa:
          type: string
        descriptionEn:
          type: string
        flag:
          $ref: '#/components/schemas/Images'

    Genre:
      type: object
      properties:
        titleFa:
          type: string
          example: نام ژانر
        titleEn:
          type: string
          example: Genre's Title
        descriptionFa:
          type: string
        descriptionEn:
          type: string     

    Song:
      required:
        - title
        - href
      type: object
      properties:
        title:
          type: string
        href:
          type: string

    Images :
      type: object
      properties:
        web:
          type: string
        phone:
          type: string

    ImageType:
       type: string
       enum:
        - slider
        - album
        - playlist
        - instrument
        - artist
        - song
        - musicvideo
        - flag
        - mood

    Instrument:
      type: object
      properties:
        titleFa:
          type: string
          example: نام ساز
        titleEn:
          type: string
          example: Instrument's Title
        descriptionFa:
          type: string
        descriptionEn:
          type: string
        thumbnail:
          $ref: '#/components/schemas/Images'

    Hashtag: 
      type: object
      properties:
        titleFa:
          type: string
          example: نام هشتگ
        titleEn:
          type: string
          example: Hashtags's Title
        descriptionFa:
          type: string
        descriptionEn:
          type: string

    Mood:
      type: object
      properties:
        titleFa:
          type: string
          example: نام حالت
        titleEn:
          type: string
          example: Mood's Title
        descriptionFa:
          type: string
        descriptionEn:
          type: string
        images:
          $ref: '#/components/schemas/Images'

    Edit :
      type: object
      properties:
        field:
          type: string
        newValue:
          type: string

    FilterByTimeWithDays:
      type: object
      properties:
        type:
          type: string
          example: time
        from:
          type: string
          example: '2020-01-01'
        days:
          type: integer
          example: 30
        
    FilterByTimeWithinAPeriod:
      type: object
      properties:
        type:
          type: string
          example: time
        from:
         type: string
         example: '2020-01-01'
        to:
          type: string
          example: '2020-03-01'
  
    FilterByGenre:
      type: object
      properties:
        type:
          type: string
          example: genre
        value:
          type: string
          example: pop

    FilterByInstrument:
      type: object
      properties:
        type:
          type: string
          example: instrument
        value:
          type: string
          example: Piano

    FilterByHashtag:
      type: object
      properties:
        type:
          type: string
          example: hashtag
        value:
          type: string
          example: newMusic

    FilterByMood:
      type: object
      properties:
        type:
          type: string
          example: mood
        value:
          type: string
          example: Sleep

    FilterTypes:
      type: string
      enum:
       - time
       - genre
       - instrument
       - hashtag
       - mood

    Sort:
      type: string
      enum:
       - newRelease
       - mostPlayed
       - mostPopular

 