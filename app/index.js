const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require('morgan');
const yaml = require('js-yaml');
const fs = require('fs')
const swaggerDoc = yaml.safeLoad(fs.readFileSync(__dirname +  '/swagger.yml'))
const swaggerUi = require("swagger-ui-express");

const { configs } = require("./values");
const {adminRoutes , applicationRoutes, websiteRoutes} = require('./routes');

const app = express();

app.use(morgan('tiny'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc))

app.use("/images", express.static("images"));
app.use("/songs", express.static("songs"));
app.use("/videos", express.static("videos"));

//Mongoose
mongoose
    .connect(configs.MONGOOSE_CONNECTION_URL, configs.MONGOOSE_CONFIG)
    .then(() => console.log("MONGOOSE CONNECTED"))
    .catch(err => console.log({ err }));


//  Admin routes
app.use("/api/v1/admin/upload/image", adminRoutes.imageUpload);
app.use("/api/v1/admin/upload/song", adminRoutes.songUpload);
app.use("/api/v1/admin/upload/video", adminRoutes.videoUpload);
app.use("/api/v1/admin/artist", adminRoutes.artist);
app.use("/api/v1/admin/single", adminRoutes.single);
app.use("/api/v1/admin/album", adminRoutes.album);
app.use("/api/v1/admin/concert", adminRoutes.concert);
app.use("/api/v1/admin/country", adminRoutes.country);
app.use("/api/v1/admin/EP", adminRoutes.EP);
app.use("/api/v1/admin/genre", adminRoutes.genre);
app.use("/api/v1/admin/hashtag", adminRoutes.hashtag);
app.use("/api/v1/admin/instrument", adminRoutes.instrument);
app.use("/api/v1/admin/mood", adminRoutes.mood);
app.use("/api/v1/admin/musicVideo", adminRoutes.musicVideo);
app.use("/api/v1/admin/slider", adminRoutes.slider);
app.use("/api/v1/admin/playlist", adminRoutes.playList);
app.use("/api/v1/admin/gallery", adminRoutes.gallery);
app.use("/api/v1/admin/songlibrary", adminRoutes.songLibrary);
app.use("/api/v1/admin/publisher", adminRoutes.publisher);


// User Website English routes
app.use("/api/v1/home" , websiteRoutes.english.homepage);
app.use("/api/v1/albums" , websiteRoutes.english.albums);


// User Website Persian routes
app.use("/api/v1/home/fa" , websiteRoutes.persian.homepage);


// User Application English routes
app.use("/api/v1/splashscreen", applicationRoutes.english.splashScreen);
// app.use("/api/v1/artist", memberApplicationRoutes.english.artist);
// app.use("/api/v1/signup", memberApplicationRoutes.signUp);
// app.use("/api/v1/single", memberApplicationRoutes.single);
// app.use("/api/v1/album", memberApplicationRoutes.album);
// app.use("/api/v1/concert", memberApplicationRoutes.concert);
// app.use("/api/v1/country", memberApplicationRoutes.country);
// app.use("/api/v1/EP", memberApplicationRoutes.EP);
// app.use("/api/v1/genre", memberApplicationRoutes.genre);
// app.use("/api/v1/hashtag", memberApplicationRoutes.hashtag);
// app.use("/api/v1/instrument", memberApplicationRoutes.instrument);
// app.use("/api/v1/mood", memberApplicationRoutes.mood);
// app.use("/api/v1/musicVideo", memberApplicationRoutes.musicVideo);
// app.use("/api/v1/slider", memberApplicationRoutes.slider);
// app.use("/api/v1/home", memberWebsiteRoutes.homepage);

// User Application Persian routes
app.use("/api/v1/splashscreen/fa", applicationRoutes.persian.splashScreen);




//Handling CORS Error
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.setHeader("Access-Control-Allow-Credentials", true);
    if (req.method === "OPTIONS") {
        res.setHeader(
            "Access-Control-Allow-Methods",
            "PUT, POST, PATCH, DELETE, GET"
        );
        return res.status(200).json({});
    }
    next();
});

app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

module.exports = app;
