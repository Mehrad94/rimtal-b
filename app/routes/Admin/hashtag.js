const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");

router.get("/:page", async (req, res) => {
  try {
    const hashtags = await models.Hashtag.paginate({}, {page: req.params.page, limit: 50});
    res.status(200).json(hashtags);
} catch (e) {
    res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
}
});

router.post("/", async (req, res) => {
  try {
    const {titleEn , titleFa , descriptionFa , descriptionEn} = req.body;
    if (!titleEn || !titleFa || !descriptionFa || !descriptionEn) res.status(406).json({ ERROR: values.errors.ER_PARAMS });
    else {
      const hashtag = await models.Hashtag({titleEn , titleFa , descriptionFa , descriptionEn}).save();
      res.status(201).json(values.success.HASHTAG_CREATED);
    }
  } catch (e) {
    res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
  }
});

module.exports = router;
