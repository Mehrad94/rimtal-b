const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");

router.get("/", async (req, res) => {
    try {
        const EPs = await models.EP.find();
        res.status(200).json(EPs);
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.post("/", async (req, res) => {
    try {
        const {title, artist, songs, genres, hashtags, released} = req.body;
        if (!title || !artist || !songs || !genres || !hashtags)
            res.status(406).json({ERROR: values.errors.ER_PARAMS});
        else {
            const EP = await models.EP({title, artist, songs, genres, hashtags, released}).save();
            await models.Artist.findOneAndUpdate({_id: artist}, {$push: {EPs: EP._id}});
            await models.Artist.findOneAndUpdate({_id: artist}, {$inc: {EPsCount: 1}});
            res.status(201).json({Code :values.success.EP_ADDED});
        }
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

module.exports = router;
