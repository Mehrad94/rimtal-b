const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");
const utils = require("../../utils");

// ===================================== Get all countries ==================
router.get("/:page", async (req, res) => {
    try {
        const countries = await models.Country.paginate({}, {page: req.params.page, limit: 50});
        res.status(200).json(countries);
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

// ===================================== search countries by name ==================
router.get("/:countryName/:page", async (req, res) => {
    const {countryName, page} = req.params;
    try {
        let searchedCountries;
        const isPersian = utils.persianRegex(countryName);
        if (isPersian){
            searchedCountries = await models.Country.paginate({titleFa : utils.escapeRegex(countryName)} , {page , limit :50});
        }else {
         searchedCountries = await models.Country.paginate({titleEn: utils.escapeRegex(countryName)}, {page, limit: 50});
        }
        res.status(200).json(searchedCountries);
    } catch (e) {
        console.log({e});
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.post("/", async (req, res) => {
    try {
        const {titleFa, titleEn, descriptionFa, descriptionEn, flag} = req.body;
        if (!(titleFa || titleEn || descriptionFa || descriptionEn || flag)) res.status(406).json({ERROR: values.errors.ER_PARAMS});
        else {
            await models.Country({titleFa, titleEn, descriptionFa, descriptionEn, flag}).save();
            res.status(201).json({CODE: values.success.COUNTRY_ADDED});
        }
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.delete("/:countryId", async (req, res) => {
//TODO check no document ref to this country if there is a document error to delete that first
    try {
        await models.Country.findByIdAndRemove(req.params.countryId);
        res.status(200).json({CODE: values.success.COUNTRY_DELETED});
    } catch (e) {
        res.status(500).json({Error: values.errors.ER_SMT_WRONG});
    }
});

module.exports = router;
