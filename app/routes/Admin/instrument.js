const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");
const utils = require("../../utils");
//===================================== Get all instrumentals ==================
router.get("/:page", async (req, res) => {
    try {
        const instrumentals = await models.Instrument.paginate({}, {page: req.params.page, limit: 50});
        res.status(200).json(instrumentals);
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

//===================================== search instrumentals by title  ==================
router.get("/:title/:page", async (req, res) => {
    const { title, page } = req.params;
    try {
        let searchedInstruments;
        const isPersian = utils.persianRegex(title);
        if (isPersian) {
            searchedInstruments = await models.Instrument.paginate({ titleFa: utils.escapeRegex(title) }, { page, limit: 50 });
        } else {
            searchedInstruments = await models.Instrument.paginate({ titleEn: utils.escapeRegex(title) }, { page, limit: 50 });
        }
        res.status(200).json(searchedInstruments);
    } catch (e) {
        console.log({e});
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

//===================================== add a new instrument  ==================
router.post("/", async (req, res) => {
    try {
        const {titleEn, titleFa, descriptionFa, descriptionEn, thumbnail} = req.body;
        if (!(titleEn || titleFa || descriptionFa || descriptionEn || thumbnail)) res.status(406).json({ERROR: values.errors.ER_PARAMS});
        else {
            await models.Instrument({titleEn, titleFa, descriptionFa, descriptionEn, thumbnail}).save();
            res.status(201).json({CODE: values.success.INSTRUMENT_ADDED});
        }
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

//===================================== delete a instrument by id  ==================
router.delete("/:instrumentId", async (req, res) => {
//TODO check no document ref to this country if there is a document error to delete that first
    try {
        await models.Instrument.findByIdAndDelete(req.params.instrumentId);
        res.status(200).json({CODE: values.success.INSTRUMENT_DELETED});
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

module.exports = router;