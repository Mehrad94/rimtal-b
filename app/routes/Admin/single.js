const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");
const moment = require('moment');

router.get("/", async (req, res) => {
    try {
        const singles = await models.Single.find().populate({
            path: "song",
            populate: {
                path: "artist"
            }
        }).limit(20);
        res.status(200).json(singles);
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.post("/", async (req, res) => {
    try {
        const {titleFa, titleEn, song, genres, artist, hashtags, images,instruments, releaseDate} = req.body;
        const date = releaseDate.year +'-'+ releaseDate.month +'-'+ releaseDate.day;
        const single = await models.Single({titleFa, titleEn, song, genres, artist, hashtags, instruments , images, releaseDate ,released:moment().isAfter(date)}).save();
        await models.Artist.findOneAndUpdate({_id: artist}, {$push: {singles: single._id} , $inc: {singlesCount: 1}});
        genres.map(async genre => {
            await models.Genre.findOneAndUpdate({_id: genre}, {$inc: {singlesCount: 1}})
        });
        moods.map(async mood => {
            await models.Mood.findOneAndUpdate({ _id: mood }, { $inc: { singlesCount: 1 } })
        })
        instruments.map(async instrument => {
            await models.Instrument.findOneAndUpdate({ _id: instrument }, { $inc: { singlesCount: 1 } })
        })
        res.status(201).json({Code: values.success.SINGLE_ADDED});
    } catch (e) {
        console.log(e)
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.put('/:id', async (req, res) => {
    try {
        const {field, newValue} = req.body;
        if (!field || !newValue)
            return res.status(500).json({Error: values.errors.ER_PARAMS});
        const edit = {};
        edit[field] = newValue;
        const editSingle = await models.Single.findOneAndUpdate(
            {_id: req.params.id},
            {$set: edit},
            {new: true}
        );
        res.status(200).json({Code: values.success.SINGLE_EDITED})
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }

});

module.exports = router;
