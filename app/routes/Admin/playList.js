const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");

router.get("/", async (req, res) => {
    try {
        const playLists = await models.PlayList.find().populate({path: 'singles', populate: {path: 'artist'}}).limit(20);
        res.status(200).json(playLists);
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.post("/", async (req, res) => {
    try {
        const {titleEn, titleFa,publisher, descriptionEn, descriptionFa, images, singles} = req.body;
        if (!titleEn || !titleFa || !descriptionEn || !descriptionFa || !images || !singles)
            res.status(406).json({ERROR: values.errors.ER_PARAMS});
        else {
            const playList = await models
                .PlayList({titleEn, titleFa, publisher,descriptionEn, descriptionFa, images, singles})
                .save();
            res.status(201).json(playList);
        }
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

module.exports = router;