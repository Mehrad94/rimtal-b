const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");

router.get("/", async (req, res) => {
    try {
        const musicVideos = await models.MusicVideo.find();
        res.status(200).json(musicVideos);
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.post("/", async (req, res) => {
    try {
        const {titleEn, titleFa, artist, images, video, genres, hashtags , releaseDate, released} = req.body;
        if (!(titleEn || !titleFa || !images || !artist || !genres || !hashtags ||!releaseDate))
            res.status(406).json({ERROR: values.errors.ER_PARAMS});
        else {
            const musicVideo = await models
                .MusicVideo({titleEn, titleFa, artist, images, video, genres, hashtags , releaseDate , released})
                .save();
            res.status(201).json(musicVideo);
        }
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.put('/:id', async (req, res) => {
    try {
        const {field, newValue} = req.body;
        if (!field || !newValue)
            return res.status(500).json({Error: values.errors.ER_PARAMS});
        const edit = {};
        edit[field] = newValue;
        const editMusicvideo = await models.MusicVideo.findOneAndUpdate(
            {_id: req.params.id},
            {$set: edit},
            {new: true}
        );
        res.status(200).json({Code: values.success.MUSICVIDEO_EDITED})
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }

});


module.exports = router;
