const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");
const utils = require("../../utils");

router.get("/:page", async (req, res) => {
    try {
        const artists = await models.Artist.paginate({}, {page: req.params.page, limit: 50});
        res.status(200).json(artists);
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.get("/search/:title/:page", async (req, res) => {
    const {title, page} = req.params;
    let searchedArtists;
    const isPersian = utils.persianRegex(title);
    if (isPersian) {
        searchedArtists = await models.Artist.paginate({nameFa: utils.escapeRegex(title)}, {page, limit: 50});
    } else {
        searchedArtists = await models.Artist.paginate({nameEn: utils.escapeRegex(title)}, {page, limit: 50});
    }
    res.status(200).json(searchedArtists);
});

router.post("/", async (req, res) => {
    try {
        const {nameFa, nameEn, avatar, defaultGenre, descriptionFa, descriptionEn} = req.body;
        if (!nameFa || !nameEn || !descriptionFa || !defaultGenre || !descriptionEn) res.status(406).json({ERROR: values.errors.ER_PARAMS});
        else {
            const artist = await models
                .Artist({nameFa, nameEn, avatar,defaultGenre, descriptionEn, descriptionFa})
                .save();
            res.status(201).json({Code: values.success.ARTIST_ADDED});
        }
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.put("/:id", async (req, res) => {
    try {
        const {field, newValue} = req.body;
        if (!field || !newValue)
            return res.status(406).json({Error: values.errors.ER_PARAMS});
        const edit = {};
        edit[field] = newValue;
        const editArtist = await models.Artist.findOneAndUpdate(
            {_id: req.params.id},
            {$set: edit},
            {new: true}
        );
        res.status(200).json({Code: values.success.ARTIST_EDITED})
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.delete("/:id", async (req, res) => {
    try {
        const artistId = req.params.id;
        await models.Artist.findOneAndDelete({_id: artistId});
        res.status(204).json({Code: values.success.ARTIST_DELETED});
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

module.exports = router;
