const express = require('express');
const router = express.Router();
const multer = require("multer");
const mkdirp = require('mkdirp');
const models = require("../../models");


const values = require("../../values");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        mkdirp('./songs/')
            .then(r => cb(null, './songs/'))
            .catch(e => cb(e, false))
    },
    filename: function (req, file, cb) {
        if (req.body.songName){cb(null, req.body.songName.replace(/\s+/g, "") + '.' + 'mp3') }
        else{
            cb(null, file.originalname.replace(/\s+/g, ""))
        }
           }
});
const fileFilter = (req, file, cb) => {
    if (file.mimetype === "audio/mp3" || file.mimetype === "audio/mpeg") cb(null, true);
    else cb(null, false);
};

const songUpload = multer({storage, fileFilter});

router.post('/', songUpload.single("song"), async (req, res) => {
    console.log({x:req.file})
    if (!req.file) res.status(500).json({CODE: values.errors.ER_PARAMS });
    const songUrl =  values.strings.BASE_URL_SERVER + req.file.path.replace(/\\/g, '/')
    const song = {title: req.file.filename, href: songUrl};
    await models.SongList(song).save();
    res.status(200).json(songUrl);
});

module.exports = router;