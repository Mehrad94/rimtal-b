const imageUpload = require("./imageUpload");
const songUpload = require("./songUpload");
const artist = require("./artist");
const single = require("./single");
const hashtag = require("./hashtag");
const album = require("./album");
const concert = require("./concert");
const country = require("./country");
const EP = require("./EP");
const genre = require("./genre");
const instrument = require("./instrument");
const mood = require("./mood");
const musicVideo = require("./musicVideo");
const slider = require("./slider");
const playList = require("./playList");
const videoUpload = require("./videoUpload");
const gallery = require("./gallery");
const songLibrary = require("./songLibrary");
const publisher = require("./publisher");

module.exports = {
  imageUpload,
  songUpload,
  artist,
  single,
  hashtag,
  album,
  country,
  concert,
  EP,
  genre,
  instrument,
  mood,
  musicVideo,
  slider,
  playList,
  videoUpload,
  gallery,
  songLibrary,
  publisher
};
