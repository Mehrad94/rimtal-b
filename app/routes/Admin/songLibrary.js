const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");
const utils = require("../../utils");
//===================================== Get all instrumentals ==================
// router.get("/:page", async (req, res) => {
//   try {
//     const instrumentals = await models.Instrument.paginate(
//       {},
//       { page: req.params.page, limit: 50 }
//     );
//     res.status(200).json(instrumentals);
//   } catch (e) {
//     res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
//   }
// });
//
// //===================================== search instrumentals by title  ==================
// router.get("/:searchedChar/:page", async (req, res) => {
//   const { searchedChar, page } = req.params;
//
//   try {
//     const searchedInstrument = await models.Instrument.paginate(
//       { titleEn: utils.escapeRegex(searchedChar) },
//       {
//         page,
//         limit: 50
//       }
//     );
//     res.status(200).json(searchedInstrument);
//   } catch (e) {
//     console.log({ e });
//     res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
//   }
// });

router.get("/:page", async (req, res) => {
    const songLibrary = await models.SongList.paginate({},
        {
            page :req.params.page,
            limit: 50,
        });
    res.status(200).json(songLibrary);
});

module.exports = router;
