const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");
const utils = require("../../utils");

router.get("/:type/:page", async (req, res) => {
  const { type, page } = req.params;
  let pictures;
  switch (type) {
    case "slider":
      pictures = await models.SliderPicture.paginate(
        {},
        {
          page,
          limit:50
        }
      );
      break;
    case "album":
      pictures = await models.AlbumPicture.paginate(
        {},
        {
          page,
          limit: 50
        }
      );
      break;
    case "playlist":
      pictures = await models.PlaylistPicture.paginate(
        {},
        {
          page,
          limit: 50
        }
      );
      break;
    case "instrument":
      pictures = await models.InstrumentThumbnail.paginate(
        {},
        {
          page,
          limit: 50
        }
      );
      break;
    case "artist":
      pictures = await models.ArtistAvatar.paginate(
        {},
        {
          page,
          limit: 50
        }
      );
      break;
    case "song":
      pictures = await models.SongPicture.paginate(
        {},
        {
          page,
          limit: 50
        }
      );
      break;
    case "musicvideo":
      pictures = await models.MusicVideoPicture.paginate(
        {},
        {
          page,
          limit: 50
        }
      );
      break;
    case "flag":
      pictures = await models.Flag.paginate(
        {},
        {
          page,
          limit: 50
        }
      );
      break;
    case "mood":
      pictures = await models.MoodPicture.paginate(
        {},
        {
          page,
          limit: 50
        }
      );
      break;
  }
  res.status(200).json(pictures);
});

module.exports = router;
