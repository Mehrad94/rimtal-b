const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");

router.get("/", async (req, res) => {
  try {
    const sliders = await models.Slide.find();
    res.status(200).json(sliders);
  } catch (e) {
    res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
  }
});

router.post("/", async (req, res) => {
  try {
    const { titleFa, titleEn, images, type, parentType, parent } = req.body;
    if (!titleFa || !titleEn || !images || !type || !parentType || !parent)
      res.status(406).json({ ERROR: values.errors.ER_PARAMS });
    else {
      const slider = await models
        .Slide({ titleFa, titleEn, images, type, parentType, parent })
        .save();
      res.status(201).json(slider);
    }
  } catch (e) {
    res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
  }
});

router.put('/:id', async (req, res) => {
  try {
    const {field, newValue} = req.body;
    if (!field || !newValue)
      return res.status(500).json({Error: values.errors.ER_PARAMS});
    const edit = {};
    edit[field] = newValue;
    const editSlider = await models.Slide.findOneAndUpdate(
        {_id: req.params.id},
        {$set: edit},
        {new: true}
    );
    res.status(200).json({Code: values.success.ALBUM_EDITED})
  } catch (e) {
    res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
  }

});

module.exports = router;
