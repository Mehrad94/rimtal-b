const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");

router.post("/", async (req, res) => {
    try {
        const {userName, firstName, lastName, gender} = req.body;
        if (!userName || !firstName || !lastName || !gender)
            res.status(406).json({ERROR: values.errors.ER_PARAMS});
        else {
            const publisher = await models
                .Publisher({userName, firstName, lastName, gender})
                .save();
            res.status(201).json(publisher);
        }
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

module.exports = router;