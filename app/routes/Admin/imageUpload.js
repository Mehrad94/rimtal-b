const express = require("express");
const fs = require("fs");
const multer = require("multer");
const models = require("../../models");
const { configs, strings, errors } = require("../../values");
const moment = require("jalali-moment");
const sharp = require("sharp");

const router = express.Router();
let imagePath = null;
let sizes = [];
let DB;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (!req.body.imageType) cb(errors.ER_PARAMS, false);
    switch (req.body.imageType) {
      case "slider":
        imagePath = configs.PATH_SLIDER_PHOTO;
        sizes = [
          (webSize = configs.SLIDER_WEB),
          (phonseSize = configs.SLIDER_PHONE),
        ];
        DB = models.SliderPicture;
        break;
      case "album":
        imagePath = configs.PATH_ALBUM_PIC;
        sizes = [
          (webSize = configs.THUMBNAIL_WEB),
          (phonseSize = configs.THUMBNAIL_PHONE),
        ];
        DB = models.AlbumPicture;
        break;
      case "playlist":
        imagePath = configs.PATH_PLAYLIST_PIC;
        sizes = [
          (webSize = configs.THUMBNAIL_WEB),
          (phonseSize = configs.THUMBNAIL_PHONE),
        ];
        DB = models.PlaylistPicture;
        break;
      case "instrument":
        imagePath = configs.PATH_INSTRUMENT_THUMBNAIL;
        sizes = [
          (webSize = configs.INSTRUMENT_WEB),
          (phonseSize = configs.INSTRUMENTS_PHONE),
        ];
        DB = models.InstrumentThumbnail;
        break;
      case "artist":
        imagePath = configs.PATH_ARTIST_AVATAR;
        sizes = [
          (webSize = configs.ARTIST_WEB),
          (phonseSize = configs.INSTRUMENTS_PHONE),
        ];
        DB = models.ArtistAvatar;
        break;
      case "song":
        imagePath = configs.PATH_SONG_PIC;
        sizes = [
          (webSize = configs.THUMBNAIL_WEB),
          (phonseSize = configs.THUMBNAIL_PHONE),
        ];
        DB = models.SongPicture;
        break;
      case "musicvideo":
        imagePath = configs.PATH_MUSIC_VIDEO_PIC;
        sizes = [
          (webSize = configs.VIDEO_THUMBNAIL_WEB),
          (phonseSize = configs.VIDEO_THUMBNAIL_PHONE),
        ];
        DB = models.MusicVideoPicture;
        break;
      case "user":
        imagePath = configs.PATH_USER_AVATAR;
        sizes = [
          (webSize = configs.USER_WEB),
          (phonseSize = configs.USER_PHONE),
        ];
        break;
      case "flag":
        imagePath = configs.PATH_FLAG;
        sizes = [
          (webSize = configs.FLAG_WEB),
          (phonseSize = configs.FLAG_PHONE),
        ];
        DB = models.Flag;
        break;
        case "mood":
          imagePath = configs.PATH_MOOD_PICTURE;
          sizes = [
            (webSize = configs.MOOD_WEB),
            (phonseSize = configs.MOOD_PHONE),
          ];
          DB = models.MoodPicture;
        break;
      default:
        cb(errors.ER_WRONG_IMAGE_TYPE, false);
        return;
    }
    if (!fs.existsSync(imagePath)) fs.mkdirSync(imagePath, { recursive: true });
    cb(null, imagePath);
  },
  filename: function (req, file, cb) {
    if (!req.body.imageName) cb(null, file.originalname.replace(/\s+/g, ""));
    else {
      cb(
        null,
        req.body.imageName.replace(/\s+/g, "") +
          "." +
          file.mimetype.split("/")[1]
      );
    }
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/png" || file.mimetype === "image/jpeg")
    cb(null, true);
  else cb(null, false);
};

const imageUpload = multer({ storage, fileFilter });

router.post("/", imageUpload.single("image"), async (req, res) => {
  try {
    let urls = [];
    const { filename: image } = req.file;
    console.log({ s: `${imagePath}/${image}` });
    console.log(req.file);
    console.log({ image });
    let i = 0;
    sizes.forEach(async (size) => {
      const imageUrl =
        strings.BASE_URL_SERVER +
        `${imagePath.substr(2)}/${
          image.split(".")[0]
        }${-size.width}${-size.height}${moment()
          .locale("fa")
          .format("-YYYYMDHHmm")}.${req.file.mimetype.split("/")[1]}`;
      urls.push(imageUrl);

      await sharp(req.file.path)
        .withMetadata()
        .resize(size)
        .toFile(
          `${imagePath}/${
            image.split(".")[0]
          }${-size.width}${-size.height}${moment()
            .locale("fa")
            .format("-YYYYMDHHmm")}.${req.file.mimetype.split("/")[1]}`
        );
      i++;
      if (sizes.length == i) {
        fs.unlinkSync(req.file.path);
      }
    });

    const imageUrls = {
      web: urls[0],
      phone: urls[1],
    };
    const galleryImage = {
      title: image,
      web: imageUrls.web,
      phone: imageUrls.phone,
    };
    await DB(galleryImage).save();
    res.status(200).json(galleryImage);
  } catch (e) {
    console.log(e);
    res.status(500).json({ ERROR: errors.ER_SMT_WRONG });
  }
});

module.exports = router;
