const express = require('express');
const router = express.Router();
const multer = require("multer");
const mkdirp = require('mkdirp');
const models = require("../../models");

const values = require("../../values");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        mkdirp('./videos/')
            .then(r => cb(null, './videos/'))
            .catch(e => cb(e, false))
    },
    filename: function (req, file, cb) {
        if (req.body.videoName){cb(null, req.body.videoName.replace(/\s+/g,'') + '.' + 'mp4') }
        else{
            cb(null, file.originalname.replace(/\s+/g,''))
        }
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === "video/mp4") cb(null, true);
    else cb(null, false);
};

const videoUpload = multer({storage, fileFilter});

//------------------------------------------- Get PhoneNumber  and is new member
router.post('/', videoUpload.single("video"), async (req, res) => {
    console.log(req.file);
    if (!req.file) res.status(500).json({CODE: values.errors.ER_PARAMS});
    const { filename: videoName } = req.file;
    const videoUrl = values.strings.BASE_URL_SERVER + req.file.path.replace(/\\/g, '/');
    const video = {title:videoName , href:videoUrl};
    await models.Video(video).save();
    res.status(200).json(videoUrl);
});

module.exports = router;