const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");

router.get("/", async (req, res) => {
  try {
    const concerts = await models.Concert.find();
    res.status(200).json(concerts);
  } catch (e) {
    res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
  }
});

router.post("/", async (req, res) => {
  try {
    const { title, artist, genres, hashtags } = req.body;
    if (!(title || artist || genres))
      res.status(406).json({ ERROR: values.errors.ER_PARAMS });
    else {
      const concert = await models
        .Concert({ title, artist, genres, hashtags })
        .save();
      res.status(201).json(concert);
    }
  } catch (e) {
    res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
  }
});

module.exports = router;
