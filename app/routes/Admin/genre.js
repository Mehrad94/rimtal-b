const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");
const utils = require("../../utils");

//===================================== Get all genre ==================
router.get("/:page", async (req, res) => {
    try {
        const genres = await models.Genre.paginate({}, {page: req.params.page, limit: 50});
        res.status(200).json(genres);
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

//===================================== search genre by title  ==================
router.get("/:searchedChar/:page", async (req, res) => {
    const {searchedChar, page} = req.params;

    try {
        const searchedGenres = await models.Genre.paginate({titleEn: utils.escapeRegex(searchedChar)}, {page, limit: 50});
        res.status(200).json(searchedGenres);
    } catch (e) {
        console.log({e});
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.post("/", async (req, res) => {
    try {
        const {titleFa, titleEn, descriptionFa, descriptionEn} = req.body;
        if (!(titleFa || titleEn || descriptionFa || descriptionEn)) res.status(406).json({ERROR: values.errors.ER_PARAMS});
        else {
            const genre = await models.Genre({titleFa, titleEn, descriptionFa, descriptionEn}).save();
            res.status(201).json(genre);
        }
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

router.delete("/:genreId", async (req, res) => {
//TODO check no document ref to this country if there is a document error to delete that first
    try {
        await models.Genre.findByIdAndDelete(req.params.genreId);
        res.status(200).json({CODE: values.success.GENRE_DELETED});
    } catch (e) {
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

module.exports = router;
