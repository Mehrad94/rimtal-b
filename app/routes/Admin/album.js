const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");
const utils = require("../../utils");
const moment = require('moment');

router.get("/", async (req, res) => {
    try {
        const albums = await models.Album.find().populate("songs genres artist");
        res.status(200).json(albums);
    } catch (e) {
        res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
    }
});

router.get("/:page", async (req, res) => {
    try {
        const albums = await models.Album.paginate({}, { page: req.params.page, limit: 50 });
        res.status(200).json(albums);
    } catch (e) {
        res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
    }
});

router.get("/search/:title/:page", async (req, res) => {
    const { title, page } = req.params;
    let searchedAlbums;
    const isPersian = utils.persianRegex(title);
    if (isPersian) {
        searchedAlbums = await models.Album.paginate({ titleFa: utils.escapeRegex(title) }, { page, limit: 50 });
    } else {
        searchedAlbums = await models.Album.paginate({ titleEn: utils.escapeRegex(title) }, { page, limit: 50 });
    }
    res.status(200).json(searchedAlbums);
});

router.post("/", async (req, res) => {
    try {
        const { titleFa, titleEn, artist, songs, countries , images, genres, hashtags,  moods, instruments ,releaseDate } = req.body;
        if (!titleFa || !titleEn || !artist || !songs || !genres ||!releaseDate)
            res.status(406).json({ ERROR: values.errors.ER_PARAMS });
        else {
            const date = releaseDate.year +'-'+ releaseDate.month +'-'+ releaseDate.day;
            const album = await models
                .Album({ titleFa, titleEn, artist, songs, countries, images, genres, moods, hashtags, instruments, releaseDate, released:moment().isAfter(date) })
                .save();
            await models.Artist.findOneAndUpdate({ _id: artist }, { $push: { albums: album._id } , $inc: {albumsCount:1} });
            genres.map(async genre => {
                await models.Genre.findOneAndUpdate({ _id: genre }, { $inc: { albumsCount: 1 } })
            });
            moods.map(async mood => {
                await models.Mood.findOneAndUpdate({ _id: mood }, { $inc: { albumsCount: 1 } })
            })
            instruments.map(async instrument => {
                await models.Instrument.findOneAndUpdate({ _id: instrument }, { $inc: { albumsCount: 1 } })
            })
            countries.map(async country => {
                await models.Country.findByIdAndUpdate({_id: country} , {$inc: {albumsCount: 1}})
            })
            hashtags.map(async hashtag =>{
                await models.Hashtag.findByIdAndUpdate({_id: hashtag} , {$inc: { albumsCount: 1}})
            })
            res.status(201).json({ Code: values.success.ALBUM_ADDED });
        }
    } catch (e) {
        res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
    }
});

router.put('/:id', async (req, res) => {
    try {
        const { field, newValue } = req.body;
        if (!field || !newValue)
            return res.status(406).json({ Error: values.errors.ER_PARAMS });
        const edit = {};
        edit[field] = newValue;
        const editAlbum = await models.Album.findOneAndUpdate(
            { _id: req.params.id },
            { $set: edit },
            { new: true }
        );
        res.status(200).json({ Code: values.success.ALBUM_EDITED })
    } catch (e) {
        res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
    }

});


router.delete("/:id", async (req, res) => {
    try {
        const albumId = req.params.id;
        await models.Album.findOneAndDelete({ _id: albumId });
        res.status(204).json({ Code: values.success.ALBUM_DELETED });
    } catch (e) {
        res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
    }
});

module.exports = router;
