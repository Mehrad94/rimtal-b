const express = require("express");
const router = express.Router();
const models = require("../../models");
const values = require("../../values");

router.get("/:page", async (req, res) => {
  try {
    const moods = await models.Mood.paginate({},{page: req.params.page , limit: 50});
    res.status(200).json(moods);
  } catch (e) {
    res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
  }
});

router.get('/:title/:page', async (req,res) => {
try {
  const { title, page } = req.params;
  let searchedMoods;
  const isPersian = utils.persianRegex(title);
  if (isPersian) {
      searchedMoods = await models.Mood.paginate({ titleFa: utils.escapeRegex(title) }, { page, limit: 50 });
  } else {
    searchedMoods = await models.Mood.paginate({ titleEn: utils.escapeRegex(title) }, { page, limit: 50 });
  }
  res.status(200).json(searchedMoods);
} catch (error) {
  
}
});

router.post("/", async (req, res) => {
  try {
    const {titleEn , titleFa , descriptionFa , descriptionEn, images} = req.body;
    if (!titleEn||!titleFa||!descriptionEn||!descriptionFa||!images) res.status(406).json({ ERROR: values.errors.ER_PARAMS });
    else {
      await models.Mood({titleEn , titleFa , descriptionFa , descriptionEn, images}).save();
      res.status(201).json(values.success.MOOD_CREATED);
    }
  } catch (e) {
    res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
  }
});

router.put('/:id', async (req, res) => {
  try {
    const {field, newValue} = req.body;
    if (!field || !newValue)
      return res.status(500).json({Error: values.errors.ER_PARAMS});
    const edit = {};
    edit[field] = newValue;
    const editMood = await models.Mood.findOneAndUpdate(
        {_id: req.params.id},
        {$set: edit},
        {new: true}
    );
    res.status(200).json({Code: values.success.MOOD_EDITED})
  } catch (e) {
    res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
  }

});

module.exports = router;
