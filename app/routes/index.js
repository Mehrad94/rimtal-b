const adminRoutes = require("./Admin");
const applicationRoutes = require("./Member/Application");
const websiteRoutes = require("./Member/Website");

module.exports = {adminRoutes, applicationRoutes, websiteRoutes};
