const english = require("./En");
const persian = require("./Fa");

module.exports = { english, persian };
