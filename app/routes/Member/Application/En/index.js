const single = require("./single");
const album = require("./album");
const artist = require("./artist");
const concert = require("./concert");
const country = require("./country");
const EP = require("./EP");
const genre = require("./genre");
const hashtag = require("./hashtag");
const instrument = require("./instrument");
const mood = require("./mood");
const musicVideo = require("./musicVideo");
const slider = require("./slider");
const splashScreen = require("./splashScreen");
const signUp = require("./signUp");

module.exports = {
  single,
  album,
  artist,
  concert,
  country,
  EP,
  genre,
  hashtag,
  instrument,
  mood,
  musicVideo,
  slider,
  splashScreen,
  signUp,
};
