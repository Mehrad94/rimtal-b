const express = require("express");
const router = express.Router();
const models = require("../../../../models");
const values = require("../../../../values");

router.get("/", async (req, res) => {
  try {
    const musicVideos = await models.MusicVideo.find();
    res.status(200).json(musicVideos);
  } catch (e) {
    res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
  }
});

module.exports = router;
