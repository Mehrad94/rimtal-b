const express = require('express');
const values = require('../../../../values');
const utils = require('../../../../utils');
const models = require('../../../../models');
const jwt = require('jsonwebtoken');

const router = express.Router();
//------------------------------------------- sign up user ===============================
//TODO validation
router.post('/', async (req, res) => {
    const {phoneNumber, userName} = req.body;
    if (!phoneNumber || phoneNumber.length !== 11) res.status(500).json({CODE: values.errors.ER_PARAMS});

    models.User.findOne({phoneNumber}, (err, user) => {
        if (err) res.status(500).json({CODE: values.errors.ER_PARAMS});
        else if (user && user.isVerified) res.status(500).json({CODE: values.errors.ER_USER_ALREADY_SIGNED_UP});
        else if (user && !user.isVerified) utils.SignUpTextMessage.smsRegister(phoneNumber, res);
        else {
            models.User({phoneNumber, userName})
                .save()
                .then(() => {
                    utils.SignUpTextMessage.smsRegister(phoneNumber, res);
                })
                .catch(e => {
                    res.status(500).json({CODE: values.errors.ER_SMT_WRONG});
                });
        }
    });
});

//------------------------------------------- validate verification code
//TODO validation
router.put('/', async (req, res) => {
    const {phoneNumber, verifyCode} = req.body;
    if (!phoneNumber || phoneNumber.length !== 11 || !verifyCode || verifyCode.length !== 6) return res.status(500).json({CODE: values.errors.ER_PARAMS});

    models.VerificationCode.findOne({phoneNumber}, (err, code) => {
        if (err) res.status(500).json({CODE: values.errors.ER_SMT_WRONG});
        else if (!code || code.verifyCode !== verifyCode) return res.status(500).json({CODE: values.errors.ER_SIGN_UP_VALIDATION});
        else {
            const token = jwt.sign({phoneNumber}, values.configs.TOKEN_SECRET, {expiresIn: "9000h"});
            models.User
                .findOneAndUpdate({phoneNumber}, {token, isVerified: true})
                .then((r) => {
                    console.log({r})
                    res.status(200).json({token})
                })
                .catch(() => res.status(500).json({CODE: values.errors.ER_SMT_WRONG}));
        }
    })
});

module.exports = router;