const express = require("express");
const router = express.Router();
const models = require("../../../../models");
const values = require("../../../../values");

router.get("/", async (req, res) => {
  try {
    const moods = await models.Mood.find();
    res.status(200).json(moods);
  } catch (e) {
    res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
  }
});

module.exports = router;
