const express = require("express");
const router = express.Router();
const models = require("../../../../models");
const values = require("../../../../values");

router.get("/",async (req,res)=>{
    try {
        const instruments = await models.Instrument.find();
        res.status(200).json(instruments);
    } catch (e) {
        res.status(500).json({ ERROR: values.errors.ER_SMT_WRONG });
    }
});



module.exports = router;