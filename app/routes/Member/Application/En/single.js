const express = require("express");
const router = express.Router();
const models = require("../../../../models");
const values = require("../../../../values");

router.get("/", async (req, res) => {
  try {
    const singles = await models.Single.find().populate("song hashtags");
    res.status(200).json(singles);
  } catch (e) {
      res.status(500).json({ERROR : values.errors.ER_SMT_WRONG})
  }
});

module.exports = router;
