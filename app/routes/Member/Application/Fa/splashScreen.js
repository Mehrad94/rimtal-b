const express = require("express");
const router = express.Router();
const models = require("../../../../models");
const values = require("../../../../values");
const utils = require("../../../../utils");


router.get("/", async (req, res) => {
    try {
        // const rawSliders = await models.Slide.find().populate({
        //     path: 'parent',
        //     select: 'titleFa -_id',
        //     populate: {path: 'artist genres', select: 'nameFa titleFa -_id'}
        // }).select('-images.web -type -titleFa');
        const rawSliders = await models.Slide.find().populate({path: 'parent', populate: {path: 'artist'}});
        let sliders = [];
        rawSliders.map(slide => {
            const newSlide = {
                title: slide.titleFa,
                image: slide.images.phone,
                parentName: slide.parent.titleFa,
                parentArtist: slide.parent.artist.nameFa,
                _id: slide._id
            };
            sliders.push(newSlide);
        });
        const rawAlbums = await models.Album.find().populate("artist");
        let resultAlbums = [];
        rawAlbums.map(album => {
            const newAlbum = {
                _id: album._id,
                title: album.titleFa,
                artist: album.artist.nameFa,
                image: album.images.phone,
            };
            resultAlbums.push(newAlbum);
        });
        const albums = utils.chunkArray(resultAlbums, 2);
        const rawSingles = await models.Single.find().populate('artist');
        let resultSingles = [];
        rawSingles.map(single => {
            const newSingle = {
                _id: single._id,
                title: single.titleFa,
                artist: single.artist.nameFa,
                image: single.images.phone,
            };
            resultSingles.push(newSingle);
        });
        const singles = utils.chunkArray(resultSingles, 2);

        const rawPlaylists = await models.PlayList.find().populate('publisher');

        let playlists = [];
        rawPlaylists.map(playlist => {
            const newPlaylist = {
                _id: playlist._id,
                image: playlist.images.phone,
                title: playlist.titleFa,
                publisher: playlist.publisher.userName,
            };
            playlists.push(newPlaylist)
        });


        const rawMusicVideos = await models.MusicVideo.find().populate('artist');

        let musicVideos = [];
        rawMusicVideos.map(musicVideo => {
            const newMusicvideo = {
                _id: musicVideo._id,
                thumbnail: musicVideo.images.phone,
                title: musicVideo.titleFa,
                artist: musicVideo.artist.nameFa,
            };
            musicVideos.push(newMusicvideo)
        });

        const rawSuggestedPlaylists = await models.PlayList.find().populate('publisher').limit(10);

        let suggestedPlaylists = [];
        rawSuggestedPlaylists.map(suggestedPlaylist => {
            const newSuggestedPlaylist = {
                _id: suggestedPlaylist._id,
                image: suggestedPlaylist.images.phone,
                title: suggestedPlaylist.titleFa,
                publisher: suggestedPlaylist.publisher.userName,
            };
            suggestedPlaylists.push(newSuggestedPlaylist)
        });

        const rawComingSoonSingles = await models.Single.find({released: false}).populate('artist genres').limit(4);

        let comingSoonSingles = [];
        rawComingSoonSingles.map(comingSoonSingle => {
            const newComingSoonSingle = {
                _id: comingSoonSingle._id,
                title: comingSoonSingle.titleFa,
                artist: comingSoonSingle.artist.nameFa,
                images: comingSoonSingle.images.phone,
            };
            comingSoonSingles.push(newComingSoonSingle);
        });

        const rawComingSoonAlbums = await models.Album.find({released: false}).populate("artist genres").limit(4);

        let comingSoonAlbums = [];
        rawComingSoonAlbums.map(comingSoonAlbum => {
            const newComingSoonAlbum = {
                _id: comingSoonAlbum._id,
                title: comingSoonAlbum.titleFa,
                artist: comingSoonAlbum.artist.nameFa,
                images: comingSoonAlbum.images.phone,
            };
            comingSoonAlbums.push(newComingSoonAlbum);
        });

        const rawComingSoonMusicVideos = await models.MusicVideo.find({released: false}).populate('artist genres').limit(4);

        let comingSoonMusicVideos = [];
        rawComingSoonMusicVideos.map(comingSoonMusicVideo => {
            const newComingSoonMusicvideo = {
                _id: comingSoonMusicVideo._id,
                images: comingSoonMusicVideo.images.phone,
                title: comingSoonMusicVideo.titleFa,
                artist: comingSoonMusicVideo.artist.nameFa,
            };
            comingSoonMusicVideos.push(newComingSoonMusicvideo)
        });

        const comingSoon = comingSoonSingles.concat(comingSoonAlbums.concat(comingSoonMusicVideos));
        const splashData = {
            sliders,
            albums,
            singles,
            playlists,
            musicVideos,
            comingSoon
        };
        res.status(200).json(splashData)
    } catch (e) {
        console.log({e});
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

module.exports = router;
