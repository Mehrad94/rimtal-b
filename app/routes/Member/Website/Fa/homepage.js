const express = require("express");
const router = express.Router();
const models = require("../../../../models");
const values = require("../../../../values");
const utils = require("../../../../utils");

router.get("/", async (req, res) => {
    try {
        const rawSliders = await models.Slide.find().populate({
            path: 'parent',
            populate: {path: 'artist genres'}
        }).limit(4);
        let sliders = [];
        rawSliders.map(slide => {
            const newSlide = {
                title: slide.titleFa,
                images: slide.images,
                parentName: slide.parent.titleFa,
                parentArtist: slide.parent.artist.nameFa,
                _id: slide._id
            };
            sliders.push(newSlide);
        });
        const rawMoods = await models.Mood.find().limit(8);
        let moods = [];
        rawMoods.map(mood => {
            const newMood = {
                title: mood.titleFa,
                images: mood.images,
                _id: mood._id
            };
            moods.push(newMood);
        });
        const rawAlbums = await models.Album.find().limit(20).populate('artist genres');
        let albums = [];
        rawAlbums.map(album => {
            const newAlbum = {
                _id: album._id,
                title: album.titleFa,
                artist: album.artist.nameFa,
                images: album.images,
                genres: album.genres.map(genre => {
                    return genre.titleFa
                }),
                releaseDate: album.releaseDate.year
            };
            albums.push(newAlbum);
        });

        const rawSingles = await models.Single.find().limit(20).populate('artist genres');

        let singles = [];
        rawSingles.map(single => {
            const newSingle = {
                _id: single._id,
                title: single.titleFa,
                artist: single.artist.nameFa,
                images: single.images,
                genres: single.genres.map(genre => {
                    return genre.titleFa
                }),
                releaseDate: single.releaseDate.year
            };
            singles.push(newSingle);
        });

        const rawPlaylists = await models.PlayList.find().populate('publisher').limit(10);

        let playlists = [];
        rawPlaylists.map(playlist => {
            const newPlaylist = {
                _id: playlist._id,
                images:playlist.images,
                title: playlist.titleFa,
                publisher: playlist.publisher.userName,
                tracksCount: playlist.singles.length,
                followersCount: playlist.followersCount,
            };
            playlists.push(newPlaylist)
        });
        const rawMusicvideos = await models.MusicVideo.find().populate('artist genres').limit(8);

        let musicVideos = [];
        rawMusicvideos.map(musicVideo => {
            const newMusicvideo = {
                _id:musicVideo._id,
                images:musicVideo.images,
                title: musicVideo.titleFa,
                artist:musicVideo.artist.nameFa,
                genres:musicVideo.genres.map(genre => {
                    return genre.titleFa
                }),
                releaseDate:musicVideo.releaseDate.year
            };
            musicVideos.push(newMusicvideo)
        });


        const rawSuggestedPlaylists = await models.PlayList.find().populate('publisher').limit(10);

        let suggestedPlaylists = [];
        rawSuggestedPlaylists.map(suggestedPlaylist => {
            const newSuggestedPlaylist = {
                _id: suggestedPlaylist._id,
                images:suggestedPlaylist.images,
                title: suggestedPlaylist.titleFa,
                publisher: suggestedPlaylist.publisher.userName,
                tracksCount: suggestedPlaylist.singles.length,
                followersCount: suggestedPlaylist.followersCount,
            };
            suggestedPlaylists.push(newSuggestedPlaylist)
        });

        const rawComingSoonSingles = await models.Single.find({released: false}).populate('artist genres').limit(4);

        let comingSoonSingles =[];
        rawComingSoonSingles.map(comingSoonSingle => {
            const newComingSoonSingle = {
                _id: comingSoonSingle._id,
                title: comingSoonSingle.titleFa,
                artist: comingSoonSingle.artist.nameFa,
                images: comingSoonSingle.images,
                genres: comingSoonSingle.genres.map(genre => {
                    return genre.titleFa
                }),
                releaseDate: comingSoonSingle.releaseDate
            };
            comingSoonSingles.push(newComingSoonSingle);
        });

        const rawComingSoonAlbums = await models.Album.find({released: false}).populate("artist genres").limit(4);

        let comingSoonAlbums = [];
        rawComingSoonAlbums.map(comingSoonAlbum => {
            const newComingSoonAlbum = {
                _id: comingSoonAlbum._id,
                title: comingSoonAlbum.titleFa,
                artist: comingSoonAlbum.artist.nameFa,
                images: comingSoonAlbum.images,
                genres: comingSoonAlbum.genres.map(genre => {
                    return genre.titleFa
                }),
                releaseDate: comingSoonAlbum.releaseDate
            };
            comingSoonAlbums.push(newComingSoonAlbum);
        });
        const rawComingSoonMusicVideos = await models.MusicVideo.find({released: false}).populate('artist genres').limit(4);

        let comingSoonMusicVideos = [];
        rawComingSoonMusicVideos.map(comingSoonMusicVideo => {
            const newComingSoonMusicvideo = {
                _id:comingSoonMusicVideo._id,
                images:comingSoonMusicVideo.images,
                title: comingSoonMusicVideo.titleFa,
                artist:comingSoonMusicVideo.artist.nameFa,
                genres:comingSoonMusicVideo.genres.map(genre => {
                    return genre.titleFa
                }),
                releaseDate:comingSoonMusicVideo.releaseDate
            };
            comingSoonMusicVideos.push(newComingSoonMusicvideo)
        });

        //const comingSoonEP = await models.EP.find({released: false}).populate("artist genres").limit(4);
        // const comingSoonConcert = await models.Concert.find({released: false}).populate('artist genres hashtags').limit(4)
        const comingSoon = comingSoonSingles.concat(comingSoonAlbums.concat(comingSoonMusicVideos));
        const homepage = {sliders, moods, albums, singles, playlists, musicVideos, suggestedPlaylists, comingSoon};
        res.status(200).json(homepage);
    } catch (e) {
        console.log(e);
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

module.exports = router;
