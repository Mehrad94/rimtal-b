const express = require("express");
const router = express.Router();
const models = require("../../../../models");
const values = require("../../../../values");
const utils = require("../../../../utils");

router.get("/", async (req, res) => {
    try {
        const rawSliders = await models.Slide.find().populate({
            path: 'parent',
            populate: {path: 'artist genres'}
        }).limit(4);
        let sliders = [];
        rawSliders.map(slide => {
            const newSlide = {
                title: slide.titleEn,
                images: slide.images,
                parentName: slide.parent.titleEn,
                parentArtist: slide.parent.artist.nameEn,
                _id: slide._id
            };
            sliders.push(newSlide);
        });
        const rawMoods = await models.Mood.find().limit(8);
        let moods = [];
        rawMoods.map(mood => {
            const newMood = {
                title: mood.titleEn,
                images: mood.images,
                _id: mood._id
            };
            moods.push(newMood);
        });
        const rawAlbums = await models.Album.find().limit(20).populate('artist genres');
        let albums = [];
        rawAlbums.map(album => {
            const newAlbum = {
                _id: album._id,
                title: album.titleEn,
                artist: album.artist.nameEn,
                images: album.images,
                genres: album.genres.map(genre => {
                    return genre.titleEn
                }),
                releaseDate: album.releaseDate.year
            };
            albums.push(newAlbum);
        });

        const rawSingles = await models.Single.find().limit(20).populate('artist genres');

        let singles = [];
        rawSingles.map(single => {
            const newSingle = {
                _id: single._id,
                title: single.titleEn,
                artist: single.artist.nameEn,
                images: single.images,
                genres: single.genres.map(genre => {
                    return genre.titleEn
                }),
                releaseDate: single.releaseDate.year
            };
            singles.push(newSingle);
        });

        const rawPlaylists = await models.PlayList.find().populate('publisher').limit(10);

        let playlists = [];
        rawPlaylists.map(playlist => {
            const newPlaylist = {
                _id: playlist._id,
                images:playlist.images,
                title: playlist.titleEn,
                publisher: playlist.publisher.userName,
                tracksCount: playlist.singles.length,
                followersCount: playlist.followersCount,
            };
            playlists.push(newPlaylist)
        });
        const rawMusicvideos = await models.MusicVideo.find().populate('artist genres').limit(8);

        let musicVideos = [];
        rawMusicvideos.map(musicVideo => {
            const newMusicvideo = {
                _id:musicVideo._id,
                images:musicVideo.images,
                title: musicVideo.titleEn,
                artist:musicVideo.artist.nameEn,
                genres:musicVideo.genres.map(genre => {
                    return genre.titleEn
                }),
                releaseDate:musicVideo.releaseDate.year
            };
            musicVideos.push(newMusicvideo)
        });


        const rawSuggestedPlaylists = await models.PlayList.find().populate('publisher').limit(10);

        let suggestedPlaylists = [];
        rawSuggestedPlaylists.map(suggestedPlaylist => {
            const newSuggestedPlaylist = {
                _id: suggestedPlaylist._id,
                images:suggestedPlaylist.images,
                title: suggestedPlaylist.titleEn,
                publisher: suggestedPlaylist.publisher.userName,
                tracksCount: suggestedPlaylist.singles.length,
                followersCount: suggestedPlaylist.followersCount,
            };
            suggestedPlaylists.push(newSuggestedPlaylist)
        });

         const rawComingSoonSingles = await models.Single.find({released: false}).populate('artist genres').limit(4);

         let comingSoonSingles =[];
        rawComingSoonSingles.map(comingSoonSingle => {
            const newComingSoonSingle = {
                _id: comingSoonSingle._id,
                title: comingSoonSingle.titleEn,
                artist: comingSoonSingle.artist.nameEn,
                images: comingSoonSingle.images,
                genres: comingSoonSingle.genres.map(genre => {
                    return genre.titleEn
                }),
                releaseDate: comingSoonSingle.releaseDate
            };
            comingSoonSingles.push(newComingSoonSingle);
        });

         const rawComingSoonAlbums = await models.Album.find({released: false}).populate("artist genres").limit(4);

        let comingSoonAlbums = [];
        rawComingSoonAlbums.map(comingSoonAlbum => {
            const newComingSoonAlbum = {
                _id: comingSoonAlbum._id,
                title: comingSoonAlbum.titleEn,
                artist: comingSoonAlbum.artist.nameEn,
                images: comingSoonAlbum.images,
                genres: comingSoonAlbum.genres.map(genre => {
                    return genre.titleEn
                }),
                releaseDate: comingSoonAlbum.releaseDate
            };
            comingSoonAlbums.push(newComingSoonAlbum);
        });
         const rawComingSoonMusicVideos = await models.MusicVideo.find({released: false}).populate('artist genres').limit(4);

        let comingSoonMusicVideos = [];
        rawComingSoonMusicVideos.map(comingSoonMusicVideo => {
            const newComingSoonMusicvideo = {
                _id:comingSoonMusicVideo._id,
                images:comingSoonMusicVideo.images,
                title: comingSoonMusicVideo.titleEn,
                artist:comingSoonMusicVideo.artist.nameEn,
                genres:comingSoonMusicVideo.genres.map(genre => {
                    return genre.titleEn
                }),
                releaseDate:comingSoonMusicVideo.releaseDate
            };
            comingSoonMusicVideos.push(newComingSoonMusicvideo)
        });

        //const comingSoonEP = await models.EP.find({released: false}).populate("artist genres").limit(4);
        // const comingSoonConcert = await models.Concert.find({released: false}).populate('artist genres hashtags').limit(4)
         const comingSoon = comingSoonSingles.concat(comingSoonAlbums.concat(comingSoonMusicVideos));
         const homepage = {sliders, moods, albums, singles, playlists, musicVideos, suggestedPlaylists, comingSoon};
        res.status(200).json(homepage);
    } catch (e) {
        console.log(e);
        res.status(500).json({ERROR: values.errors.ER_SMT_WRONG});
    }
});

module.exports = router;
