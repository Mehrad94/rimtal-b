const homepage = require("./homepage");
const albums = require("./albums");


module.exports = {
    homepage,
    albums
};
