const express = require("express");
const router = express.Router();
const models = require("../../../../models");
const values = require("../../../../values");
const utils = require("../../../../utils");
const moment = require("moment");

router.get("/:page", async (req, res) => {
  const rawAlbums = await models.Album.find()
    .populate([
      { path: "artist", select: "nameEn -_id" },
      { path: "moods", select: "titleEn -_id" },
      { path: "genres", select: "titleEn -_id" },
      { path: "hashtags", select: "title -_id" },
    ])
    .select("-titleFa -songs -__v");
  let albums = rawAlbums.map((album) => {
    return {
      _id: album._id,
      title: album.titleEn,
      images: album.images,
      artist: album.artist.nameEn,
      genres: album.genres.map((genre) => {
        return genre.titleEn;
      }),
      moods: album.moods.map((mood) => {
        return mood.titleEn;
      }),
      hashtags: album.hashtags.map((hashtag) => {
        return hashtag.title;
      }),
      releaseDate: album.releaseDate.year,
    };
  });

  const paginatedAlbums = utils.paginate(albums, 50, req.params.page);
  res.json(paginatedAlbums);
});

router.post("/:page", async (req, res) => {
  let filteredAlbums = [];
  const { filters, sort } = req.body;

  const rawAlbums = await models.Album.find()
    .populate([
      { path: "artist", select: "nameEn -_id" },
      { path: "moods", select: "titleEn -_id" },
      { path: "genres", select: "titleEn -_id" },
      { path: "hashtags", select: "title -_id" },
      { path: "instruments", select: "titleEn -_id" },
    ])
    .select("-titleFa -songs -__v");

  let albums = rawAlbums.map((album) => {
    return {
      _id: album._id,
      title: album.titleEn,
      images: album.images,
      artist: album.artist.nameEn,
      genres: album.genres.map((genre) => {
        return genre.titleEn;
      }),
      moods: album.moods.map((mood) => {
        return mood.titleEn;
      }),
      hashtags: album.hashtags.map((hashtag) => {
        return hashtag.title;
      }),
      instruments: album.instruments.map((instrument) => {
        return instrument.titleEn;
      }),
      releaseDate: album.releaseDate,
      playCount: album.playCount,
      rating: album.rating,
      createdAt: album.createdAt,
    };
  });

  if (filters) {
    filters.forEach((filter) => {
      console.log(filter);
      switch (filter.type) {
        case "time":
          let filteredAlbumsByCreatedTime;
          if (filteredAlbums.length > 0) {
            filteredAlbumsByCreatedTime = utils.filterByCreatedTime({
              array: filteredAlbums,
              from: filter.from,
              to: filter.to,
              days: filter.days,
            });
          } else {
            filteredAlbumsByCreatedTime = utils.filterByCreatedTime({
              array: albums,
              from: filter.from,
              to: filter.to,
              days: filter.days,
            });
          }
          filteredAlbums = filteredAlbumsByCreatedTime;
          break;
        case "genre":
          let filteredAlbumsByGenre;
          if (filteredAlbums.length > 0) {
            filteredAlbumsByGenre = utils.filterByGenre({
              array: filteredAlbums,
              genre: filter.value,
            });
          } else {
            filteredAlbumsByGenre = utils.filterByGenre({
              array: albums,
              genre: filter.value,
            });
          }
          filteredAlbums = filteredAlbumsByGenre;
          break;
        case "instrument":
          let filteredAlbumsByInstrument;
          if (filteredAlbums.length > 0) {
            filteredAlbumsByInstrument = utils.filterByInstrument({
              array: filteredAlbums,
              instrument: filter.value,
            });
          } else {
            filteredAlbumsByInstrument = utils.filterByInstrument({
              array: albums,
              instrument: filter.value,
            });
          }
          filteredAlbums = filteredAlbumsByInstrument;
          break;
        case "hashtag":
          let filteredAlbumsByHashtag;
          if (filteredAlbums.length > 0) {
            filteredAlbumsByHashtag = utils.filterByHashtag({
              array: filteredAlbums,
              hashtag: filter.value,
            });
          } else {
            filteredAlbumsByHashtag = utils.filterByHashtag({
              array: albums,
              hashtag: filter.value,
            });
          }
          filteredAlbums = filteredAlbumsByHashtag;
          break;
        case "mood":
          let filteredAlbumsByMood;
          if (filteredAlbums.length > 0) {
            filteredAlbumsByMood = utils.filterByMood({
              array: filteredAlbums,
              mood: filter.value,
            });
          } else {
            filteredAlbumsByMood = utils.filterByMood({
              array: albums,
              mood: filter.value,
            });
          }
          filteredAlbums = filteredAlbumsByMood;
          break;
      }
    });
  }

  if (sort) {
    if (filteredAlbums.length > 0) {
      utils.sortByType(filteredAlbums, sort.type);
      console.log({z:"filter",x:sort.type});
      
    } else {
      utils.sortByType(albums, sort.type);
      console.log({z:"noFil",x:sort.type});

    }
  }

  let paginatedAlbums;
  if (filteredAlbums.length > 0) {
    paginatedAlbums = utils.paginate(filteredAlbums, 50, req.params.page);
  } else {
    paginatedAlbums = utils.paginate(albums, 50, req.params.page);
  }

  res.json(paginatedAlbums);
});

module.exports = router;
