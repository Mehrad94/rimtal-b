const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const instrumentThumbnailSchema = mongoose.Schema({
    title:{type: String},
    web:{type: String},
    phone:{type: String}
});

mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("InstrumentThumbnail" , instrumentThumbnailSchema);
