const mongoose = require("mongoose");

const EPSchema = mongoose.Schema({
    title: {type: String},
    artist: {type: mongoose.Schema.Types.ObjectId, ref: "Artist"},
    songs: [
        {type: mongoose.Schema.Types.ObjectId, ref: "Song", required: true}
    ],
    genres: [{type: mongoose.Schema.Types.ObjectId, ref: "Genre"}],
    downloadCount: {type: Number, default: 0},
    playedCount: {type: Number, default: 0},
    boughtCount: {type: Number, default: 0},
    hashtags: [{type: mongoose.Schema.Types.ObjectId, ref: "Hashtag"}],
    releaseDate: {type: String},
    released: {type: Boolean, default: true}
});

module.exports = mongoose.model("EP", EPSchema);
