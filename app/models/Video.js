const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const videoSchema = mongoose.Schema({
    title:{type: String},
    href:{type: String}
});

mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("Video" , videoSchema);
