const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  userName: { type: String, required: true },
  firstName: { type: String },
  lastName: { type: String },
  phoneNumber: { type: String },
  avatar: { type: String },
  token: [{type: String}],
  isVerified: {type: Boolean, default: false},
  birthday: { type: String },
  gender: { type: String, enum: ["Male", "Female"] },
  planType: {},
  followingPlaylists:[{}],
  followingArtists:[{}],
  isBlocked: { type: Boolean, default: false },
  signUpDate: { type: String },
  planExpirationDate: { type: String }
});

module.exports = mongoose.model("User", userSchema);
