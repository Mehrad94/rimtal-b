const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const slideSchema = mongoose.Schema({
  titleEn: { type: String, required: true },
  titleFa: { type: String, required: true },
  // artist: {type: mongoose.Schema.Types.ObjectId, ref: "Artist"},
  images: {
    web: { type: String, required: true },
    phone: { type: String, required: true }
  },
  type: { type: String, required: true, enum: ["HOME"] },
  parentType: {
    type: String,
    required: true,
    enum: ["Album", "EP", "Single", "MusicVideo"]
  },
  parent: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    refPath: "parentType"
  }
});

mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("Slide", slideSchema);
