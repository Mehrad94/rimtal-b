const mongoose = require("mongoose");

const playListSchema = mongoose.Schema({
    titleEn: {type: String , required: true},
    titleFa: {type: String , required: true},
    descriptionEn: {type: String , required: true},
    descriptionFa: {type: String , required: true},
    images: {
        web: { type: String, required: true },
        phone: { type: String, required: true }
    },
    publisher: {type: mongoose.Schema.Types.ObjectId , ref:"Publisher"},
    singles: [
        {type: mongoose.Schema.Types.ObjectId, ref: "Single", required: true}
    ],
    followersCount:{type: Number, default: 0},
    downloadCount: {type: Number, default: 0},
    playedCount: {type: Number, default: 0},
    boughtCount: {type: Number, default: 0},
});

module.exports = mongoose.model("PlayList", playListSchema);
