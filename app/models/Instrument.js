const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const instrumentSchema = mongoose.Schema({
    titleEn: {type: String, required: true},
    titleFa: {type: String, required: true},
    descriptionFa: {type: String},
    descriptionEn: {type: String},
    thumbnail: {
        phone: { type: String },
        web: { type: String }
    },
    albumsCount: {type: Number, default: 0},
    singlesCount: {type: Number, default: 0},
    musicVideoCounts: {type: Number, default: 0},
    followersCount: {type: Number, default: 0},
    playedCount: {type: Number, default: 0}
});

mongoose.plugin(mongoosePaginate);
instrumentSchema.index({titleEn: "text"}, {weights: {titleEn: 5}});
module.exports = mongoose.model("Instrument", instrumentSchema);
