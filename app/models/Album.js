const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const albumSchema = mongoose.Schema({
  titleFa: { type: String },
  titleEn: { type: String },
  artist: { type: mongoose.Schema.Types.ObjectId, ref: "Artist" },
  images: {
    phone: { type: String },
    web: { type: String }
  },
  songs: [
    { type: mongoose.Schema.Types.ObjectId, ref: "SongList", required: true }
  ],
  moods: [{ type: mongoose.Schema.Types.ObjectId, ref: "Mood" }],
  hashtags: [{ type: mongoose.Schema.Types.ObjectId, ref: "Hashtag" }],
  instruments: [{ type: mongoose.Schema.Types.ObjectId, ref: "Instrument" }],
  genres: [{ type: mongoose.Schema.Types.ObjectId, ref: "Genre" }],
  releaseDate: { year: { type: String, default: 0 }, month: { type: String, default: 0 }, day: { type: String, default: 0 } },
  released: { type: Boolean, default: true },
  countries: [{type: mongoose.Schema.Types.ObjectId, ref: "Country"}],
  playCount: { type: Number },
  rating: { type: Number , max:5 }
}, { timestamps: true });

mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("Album", albumSchema);
