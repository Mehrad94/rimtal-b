const mongoose = require("mongoose");

const singleSchema = mongoose.Schema({
    titleFa: {type: String},
    titleEn: {type: String},
    song: {type: mongoose.Schema.Types.ObjectId, required: true, ref: "SongList"},
    artist: {type: mongoose.Schema.Types.ObjectId, ref: "Artist", required: true},
    images: {
        phone: {type: String},
        web: {type: String}
    },
    genres: [{type: mongoose.Schema.Types.ObjectId, ref: "Genre"}],
    moods: [{ type: mongoose.Schema.Types.ObjectId, ref: "Mood" }],
    instruments: [{ type: mongoose.Schema.Types.ObjectId, ref: "Instruments" }],
    downloadCount: {type: Number, default: 0},
    playedCount: {type: Number, default: 0},
    boughtCount: {type: Number, default: 0},
    hashtags: [{type: mongoose.Schema.Types.ObjectId, ref: "Hashtag"}],
    releaseDate: {year: {type: String, default: 0}, month: {type: String, default: 0}, day: {type: String, default: 0}},
    released: {type: Boolean, default: true}
});

module.exports = mongoose.model("Single", singleSchema);