const mongoose = require("mongoose");

const publisherSchema = mongoose.Schema({
    userName: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    phoneNumber: { type: String },
    avatar: { type: String},
    birthday: { type: String },
    gender: { type: String, required: true, enum: ["Male", "Female"] },
    planType: {},
    isBlocked: { type: Boolean, default: false },
    signUpDate: { type: String },
    planExpirationDate: { type: String }
});

module.exports = mongoose.model("Publisher", publisherSchema);
