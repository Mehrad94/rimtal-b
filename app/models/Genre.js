const mongoose = require('mongoose');
const mongoosePaginate = require("mongoose-paginate");

const genreSchema = mongoose.Schema({
    titleFa: {type: String},
    titleEn: {type: String},
    followersCount: {type: Number, default: 0},
    descriptionFa: {type: String},
    descriptionEn: {type: String},
    albumsCount: {type: Number, default: 0},
    singlesCount: {type: Number, default: 0},
    musicVideoCount: {type: Number, default: 0},
    playedCount: {type: Number, default: 0}
});

mongoose.plugin(mongoosePaginate);
genreSchema.index({titleEn: "text"}, {weights: {titleEn: 5}});
module.exports = mongoose.model('Genre', genreSchema);