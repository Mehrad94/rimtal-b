const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const moodSchema = mongoose.Schema({
  titleEn: { type: String, required: true },
  titleFa: { type: String, required: true },
  descriptionFa: {type: String},
  descriptionEn: {type: String},
  images: {
    web: { type: String, required: true },
    phone: { type: String, required: true }
  },
  followersCount: {type: Number, default: 0},
  albumsCount: {type: Number, default: 0},
  singlesCount: {type: Number, default: 0},
  playedCount: {type: Number, default: 0}
});

mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("Mood", moodSchema);
