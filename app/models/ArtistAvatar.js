const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const artistAvatarSchema = mongoose.Schema({
    title:{type: String},
    web:{type: String},
    phone:{type: String}
});

mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("ArtistAvatar" , artistAvatarSchema);
