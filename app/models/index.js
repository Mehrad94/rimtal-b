const Artist = require("./Artist");
const Single = require("./Single");
const Album = require("./Album");
const Concert = require("./Concert");
const Country = require("./Country");
const EP = require("./EP");
const Genre = require("./Genre");
const Hashtag = require("./Hashtag");
const Instrument = require("./Instrument");
const Mood = require("./Mood");
const MusicVideo = require("./MusicVideo");
const Config = require("./Config");
const Slide = require("./Slide");
const User = require("./User");
const PlayList = require("./PlayList");
const VerificationCode = require("./VerificationCode");
const InstrumentThumbnail = require("./InstrumentThumbnail");
const SongPicture = require("./SongPicture");
const PlaylistPicture = require("./PlaylistPicture");
const Flag = require("./Flag");
const AlbumPicture = require("./AlbumPicture");
const ArtistAvatar = require("./ArtistAvatar");
const MusicVideoPicture = require("./MusicVideoPicture");
const SongList = require("./SongList");
const MoodPicture = require("./MoodPicture");
const Publisher = require("./Publisher");
const Video = require("./Video");
const SliderPicture = require('./SliderPicture');

module.exports = {
  Artist,
  Single,
  Album,
  Concert,
  Country,
  EP,
  Genre,
  Hashtag,
  Instrument,
  Mood,
  User,
  MusicVideo,
  Config,
  Slide,
  PlayList,
  VerificationCode,
  InstrumentThumbnail,
  SongPicture,
  PlaylistPicture,
  Flag,
  AlbumPicture,
  ArtistAvatar,
  MusicVideoPicture,
  SongList,
  MoodPicture,
  Publisher,
  Video,
  SliderPicture
};
