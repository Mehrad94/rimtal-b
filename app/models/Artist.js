const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const artistSchema = mongoose.Schema({
  nameFa: { type: String, required: true },
  nameEn: { type: String, required: true },
  avatar: { phone: { type: String }, web: { type: String } },
  descriptionFa: { type: String, required: true },
  descriptionEn: { type: String, required: true },
  birthday: {
    year: { type: String, default: 0 },
    month: { type: String, default: 0 },
    day: { type: String, default: 0 },
  },
  defaultGenre: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Genre",
    required: true,
  },
  genres: [{ type: mongoose.Schema.Types.ObjectId, ref: "Genre" }],
  birthPlaceCity: { type: String, default: null },
  birthPlaceCountry: { type: mongoose.Schema.Types.ObjectId, ref: "Country" },
  gender: { type: String, enum: ["MALE", "FEMALE"] },
  instruments: [{ type: mongoose.Schema.Types.ObjectId, ref: "Instrument" }],
  website: { type: String },
  instagram: { type: String },
  youTube: { type: String },
  twitter: { type: String },

  // Database Relations
  albums: [{ type: mongoose.Schema.Types.ObjectId, ref: "Album" }],
  albumsCount: { type: Number, default: 0 },
  singles: [{ type: mongoose.Schema.Types.ObjectId, ref: "Single" }],
  singlesCount: { type: Number, default: 0 },
  musicVideos: [{ type: mongoose.Schema.Types.ObjectId, ref: "MusicVideo" }],
  EPs: [{ type: mongoose.Schema.Types.ObjectId, ref: "EP" }],
  EPsCount: { type: Number, default: 0 },
  likeCount: { type: Number, default: 0 },
  followersCount: { type: Number, default: 0 },
  listenCount: { type: Number, default: 0 }, // when each played inc 1 (Singles,Albums,...)
  viewCount: { type: Number, default: 0 }, //every time get artist page inc 1
  number: { type: Number },
});

mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("Artist", artistSchema);
