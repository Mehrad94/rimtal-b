const mongoose = require("mongoose");

const concertSchema = mongoose.Schema({
    title:{type: String},
    artist:{type: mongoose.Schema.Types.ObjectId , ref:"Artist"},
    genres:[{type: mongoose.Schema.Types.ObjectId , ref:"Genre"}],
    downloadCount:{type: Number, default: 0 },
    playedCount:{type: Number, default: 0 },
    boughtCount:{type: Number, default: 0 },
    hashtags:[{type: mongoose.Schema.Types.ObjectId , ref:"Hashtag"}],
    released: { type: Boolean, default: true }
});

module.exports = mongoose.model("Concert" , concertSchema);
