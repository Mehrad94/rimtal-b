const mongoose = require("mongoose");

const VerificationCode = mongoose.Schema(
    {
        phoneNumber: {type: String},
        verifyCode: {type: String},
    },    {timestamps: true}
);

module.exports = mongoose.model("VerificationCode", VerificationCode);