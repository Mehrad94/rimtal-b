const mongoose = require("mongoose");

const musicVideoSchema = mongoose.Schema({
  titleEn: { type: String },
  titleFa: { type: String },
  video: { type: String  ,required: true},
  images: {
    web: { type: String, required: true },
    phone: { type: String, required: true }
  },
  artist: { type: mongoose.Schema.Types.ObjectId, ref: "Artist" },
  genres: [{ type: mongoose.Schema.Types.ObjectId, ref: "Genre" }],
  downloadCount: { type: Number, default: 0 },
  playedCount: { type: Number, default: 0 },
  boughtCount: { type: Number, default: 0 },
  hashtags: [{ type: mongoose.Schema.Types.ObjectId, ref: "Hashtag" }],
  releaseDate: {year: {type: String, default: 0}, month: {type: String, default: 0}, day: {type: String, default: 0}},
  released: { type: Boolean, default: true }
});

module.exports = mongoose.model("MusicVideo", musicVideoSchema);
