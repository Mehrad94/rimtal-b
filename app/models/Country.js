const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const countrySchema = mongoose.Schema({
  titleFa: { type: String },
  titleEn: { type: String },
  flag: { phone: { type: String }, web: { type: String } },
  descriptionFa: { type: String },
  descriptionEn: { type: String },
  albumsCount: { type: Number, default: 0 },
  singlesCount: { type: Number, default: 0 },
  musicVideoCounts: { type: Number, default: 0 },
  playedCount: { type: Number, default: 0 },
});

mongoose.plugin(mongoosePaginate);
countrySchema.index(
  { titleFa: "text", titleEn: "text" },
  { weights: { titleFa: 5, titleEn: 5 } }
);
module.exports = mongoose.model("Country", countrySchema);
