const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const flagSchema = mongoose.Schema({
    title:{type: String},
    web:{type: String},
    phone:{type: String}
});

mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("Flag" , flagSchema);
