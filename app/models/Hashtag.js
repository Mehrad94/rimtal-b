const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const hashtagSchema = mongoose.Schema({
    titleEn:{type: String},
    titleFa:{type: String},
    descriptionFa: {type: String},
    descriptionEn: {type: String},
    followersCount: {type: Number, default: 0},
    albumsCount: {type: Number, default: 0},
    singlesCount: {type: Number, default: 0},
    musicVideoCount: {type: Number, default: 0},
    playedCount: {type: Number, default: 0}
});

mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("Hashtag" , hashtagSchema);
