const mongoose = require("mongoose");

const songListSchema = mongoose.Schema({
    title:{type:String , required: true},
    href:{type: String ,required:true}
});

module.exports= mongoose.model('SongList' , songListSchema);
