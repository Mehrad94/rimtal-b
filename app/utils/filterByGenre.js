
module.exports = ({ array, genre }) => {
let filteredByGenre = [];
  
  array.map((element) => {
    if (element.genres.includes(genre)) {
      filteredByGenre.push(element);
    }
  });
  return filteredByGenre;
};
