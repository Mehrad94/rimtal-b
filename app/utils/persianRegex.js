const persianRegex = /^[\u0600-\u06FF\uFB8A\u067E\u0686\u06AF\s]+$/g;

module.exports = (text) =>{
    return persianRegex.test(text)
};