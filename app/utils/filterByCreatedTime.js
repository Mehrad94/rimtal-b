const moment = require("moment");


module.exports = ({ array, from, to, days }) => {
  let filterByCreatedTime = [];
  let endAt;
  if (days) endAt = moment(from).add(days, "days").format("YYYY-MM-DD HH:mm:ss");
  else endAt = moment(to).format("YYYY-MM-DD HH:mm:ss");
  array.map((element) => {    
    console.log(moment(element.createdAt).format("YYYY-MM-DD HH:mm:ss")) 

    if (moment(element.createdAt).isBetween(from, endAt)) {
      filterByCreatedTime.push(element);
    }
  });
  return filterByCreatedTime;
};
