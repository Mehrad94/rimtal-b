
module.exports = ({ array, mood }) => {
  let filteredByMood = [];
  array.map((element) => {
    if (element.moods.includes(mood)) {
      filteredByMood.push(element);
    }
  });
  return filteredByMood;
};
