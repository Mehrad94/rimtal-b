const dateFormat = require("./date");
const moment = require("moment");

module.exports = (array, type) => {
  let sortByType = [];

  switch (type) {
    case "newRelease":
      sortByType = array.sort((a, b) =>
        moment(dateFormat(a)).isSameOrBefore(dateFormat(b)) ? 1 : -1
      );
      break;
    case "mostPlayed":
      sortByType = array.sort((a, b) =>
        a.playCount > b.playCount ? 1 : -1
      );
      break;

    case "mostPopular":
      sortByType = array.sort((a, b) => (a.rating > b.rating ? -1 : 1));
      break;

    case "paidAlbums":
      break;
  }
  console.log({sortByType});
  
  return sortByType;
};
