
module.exports = ({ array, hashtag }) => {
  let filteredByHashtag = [];
  array.map((element) => {
    if (element.hashtags.includes(hashtag)) {
      filteredByHashtag.push(element);
    }
  });
  return filteredByHashtag;
};
