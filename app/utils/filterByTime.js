const dateFormat = require("./date");
const moment = require("moment");


module.exports = ({ array, from, to, days }) => {
  let filterByTime = [];
  let endAt;
  if (days) endAt = moment(from).add(days, "days").format("YYYY-MM-DD");
  else endAt = moment(to).format("YYYY-MM-DD");
  array.map((element) => {    
    date = dateFormat(element);
    if (moment(date).isBetween(from, endAt)) {
      filterByTime.push(element);
    }
  });
  return filterByTime;
};
