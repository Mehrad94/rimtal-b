module.exports = (property) => {
  return (
    property.releaseDate.year +
    "-" +
    property.releaseDate.month +
    "-" +
    property.releaseDate.day
  );
};
