
module.exports = ({ array, instrument }) => {
  let filteredByInstrument = [];
  array.map((element) => {
    if (element.instruments.includes(instrument)) {
      filteredByInstrument.push(element);
    }
  });
  return filteredByInstrument;
};
