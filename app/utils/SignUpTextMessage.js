const models = require('../models');
const values = require('../values');
const rp = require("request-promise");


const sendVerificationCodeSms = (verifyCode, receptor, res) => {
    rp({
        method: "POST",
        uri: values.configs.KN_PANEL_URL,
        form: {receptor, token: verifyCode, template: 'registerUser'},
        headers: {"content-type": "application/x-www-form-urlencoded"}
    }).then(async () => {
        await models.VerificationCode({phoneNumber: receptor, verifyCode}).save();
        res.status(200).json({CODE: values.success.TEXT_MESSAGE_SENT});
    }).catch((err) => {
        console.log({err});
        res.status(500).json({CODE: values.errors.ER_SMT_WRONG});
    });
};

const smsRegister = async (receptor, res) => {
    const verifyCode = Math.floor(100000 + Math.random() * 900000);
    const foundedCode = await models.VerificationCode.findOne({phoneNumber: receptor});
    if (foundedCode) {
        const timePast = Date.now() - foundedCode.createdAt;
        if (timePast > 120000) {
            await models.VerificationCode.findOneAndDelete({phoneNumber: receptor});
            sendVerificationCodeSms(verifyCode, receptor, res);
        } else {
            res.status(200).json({CODE: values.errors.ER_TEXT_MESSAGE_ALREADY_SENT});
        }
    } else {
        sendVerificationCodeSms(verifyCode, receptor, res);
    }

};

module.exports = {smsRegister};
