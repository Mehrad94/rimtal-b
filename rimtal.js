const http = require('http');
const app = require('./app');
const port = process.env.port || 8443;
const server = http.createServer(app);

server.listen(port, function (err) {
    if (err) console.log(err);
    else console.log("Server started on Port:" + port);
});